import { Injectable } from '@angular/core';
import { JwtHttp } from 'ng2-ui-auth';
import 'rxjs/add/operator/map';
import { Config } from './config';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class FavorTypeService {

  public urlBase: string = Config.ApiUrl;

  constructor(public http: HttpClient) {

  }

  list(): Promise<any> {
    return this.http.get([this.urlBase, 'fvfavortype'].join('/')).toPromise();
  };
}
