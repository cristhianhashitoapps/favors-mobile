import { Injectable } from '@angular/core';
import { JwtHttp } from 'ng2-ui-auth';
import { Config } from './config';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserFactory {

  public urlBase: string = Config.ApiUrl;

  constructor(public http: HttpClient) {

  }

  UpdateUser(id, usr) {
    return this.http.put([this.urlBase, 'fvuser', id].join('/'), usr).map(res => res);
  };

  GetUser(id) {
    return this.http.get([this.urlBase, 'fvuser', id].join('/')).map(res => res);
  };
  getImage(id) {
    return this.http.get([this.urlBase, 'fvusers', id, "img"].join('/'), { responseType: "text" }).toPromise();
  };

  GetUsers() {
    return this.http.get([this.urlBase, 'fvuser'].join('/')).map(res => res);
  };

  SignUp(usr) {
    return this.http.post([this.urlBase, 'fvuser', 'signup'].join('/'), usr).map(res => res);
  };

  Login(usr) {
    return this.http.post([this.urlBase, 'fvuser', 'login'].join('/'), usr).map(res => res);
  };

  RecoverPassword(email){
    return this.http.get([this.urlBase, 'fvuser',email, 'recoverpass'].join('/')).map(res => res);
  }

}
