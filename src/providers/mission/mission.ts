import { Injectable } from '@angular/core';
import { JwtHttp } from 'ng2-ui-auth';
import { Config } from '../config';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class MissionProvider {

  public urlBase: string = Config.ApiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello MissionProvider Provider');
  }

  GetMissionsPossibleTaker() {
    return this.http.get([this.urlBase, 'fvposibletaker'].join('/')).map(res => res);
  };

  GetMissions() {
    return this.http.post([this.urlBase, 'fvfavor/allactive/'].join('/'),null).map(res => res);
  };

  CreateNomination(data) {
    return this.http.post([this.urlBase, 'fvposibletaker'].join('/'), data).map(res => res);
  }

  AddTypesToMission(data) {
    return this.http.post([this.urlBase, 'fvfavorandtype'].join('/'), data).map(res => res);
  }

  updateMission(mission) {
    return this.http.put([this.urlBase, 'fvfavor', mission.id].join('/'), mission).map(res => res);
  }

  GetMissionsPossibleTakerBySalvadiaId(data) {
    return this.http.get([this.urlBase, 'fvposibletaker',data,'getposibletakerbysalvadiaid'].join('/')).map(res => res);
  };

}
