import { Injectable } from '@angular/core';
import { JwtHttp } from 'ng2-ui-auth';
import { Config } from '../config';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TransactionStateProvider {

  public urlBase: string = Config.ApiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello TransactionStateProvider Provider');
  }

  GetTransactions() {
    return this.http.get([this.urlBase, 'fvtransactionstate'].join('/')).map(res => res);
  };

}
