import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { CreditCardDto } from '../dto/CreditCardDto';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Config } from './config';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CuponProvider {

  public urlBase: string = Config.ApiUrl;

  constructor(public http: HttpClient) {
  } 

  GetByName(name) {
    return this.http.get([this.urlBase, 'fvcupon',name,'getbyname'].join('/')).toPromise();
  }

  GetByUserAndCupon(cupon,userId) {
    var body={
      "cupon":cupon,
      "FvuserId":userId
    }
    return this.http.post([this.urlBase, 'fvcuponuser','getbycuponuser'].join('/'),body).toPromise();
  }

  CreateCuponForUser(cupon,userId){
    var body={
      "cupon":cupon,
      "FvuserId":userId
    }
    return this.http.post([this.urlBase, 'fvcuponuser'].join('/'),body).toPromise();
  }

}
