import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { CreditCardDto } from '../dto/CreditCardDto';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Config } from './config';
import { AddressDto } from '../dto/AddressDto';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class AddressProvider {

  public urlBase: string = Config.ApiUrl;

  constructor(public http: HttpClient) {

  }

  GetAddress(id: number) {
    return this.http.get([this.urlBase, 'fvaddresses', id, 'GetById'].join('/')).map(res => res);
  }

  findById(id: number) {
    return this.http.get([this.urlBase, 'fvaddresses', id].join('/')).map(res => res);
  }

  GetAddresses() {
    return this.http.get([this.urlBase, 'fvaddresses'].join('/')).map(res => res);
  };

  list(): Promise<any> {
    return this.http.get([this.urlBase, 'fvaddresses'].join('/')).toPromise();
  };

  create(addressDto: AddressDto): Observable<any> {
    return this.http.post([this.urlBase, 'fvaddresses'].join('/'), addressDto);
  };

}
