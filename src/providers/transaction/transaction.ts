import { Injectable } from '@angular/core';
import { JwtHttp } from 'ng2-ui-auth';
import { Config } from '../config';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';

/*
  Generated class for the TransactionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TransactionProvider {

  public urlBase: string = Config.ApiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello TransactionProvider Provider');
  }

  GetTransactions() {
    return this.http.get([this.urlBase, 'fvtransaction'].join('/')).map(res => res);
  };

  GetTransaction(id) {
    return this.http.get([this.urlBase, 'fvtransaction', id, 'getcomplete'].join('/')).map(res => res);
  };

  CreateTransaction(data) {
    return this.http.post([this.urlBase, 'fvtransaction'].join('/'), data).map(res => res);
  };

  GetTransactionsByUserId(userId) {
    return this.http.get([this.urlBase, 'fvtransaction',userId,'getbyuserid'].join('/')).map(res => res);
  };

  GetTransactionsByFavorId(favorId) {
    return this.http.get([this.urlBase, 'fvtransaction',favorId,'getbyfavorid'].join('/')).map(res => res);
  };

}
