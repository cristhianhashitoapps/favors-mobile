import { Injectable } from '@angular/core';
import { JwtHttp } from 'ng2-ui-auth';
import { Config } from '../config';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';

/*
  Generated class for the TypesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
@Injectable()
export class TypesProvider {

  public urlBase: string = Config.ApiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello TypesProvider Provider');
  }

  GetTypes() {
    return this.http.get([this.urlBase, 'fvfavortype'].join('/')).map(res => res);
  };

  GetMissionTypes() {
    return this.http.get([this.urlBase, 'fvfavorandtype'].join('/')).map(res => res);
  };

  GetMissionTypesById(id: number) {
    return this.http.get([this.urlBase, 'fvfavorandtype', id, 'GetById'].join('/')).map(res => res);
  };

}
