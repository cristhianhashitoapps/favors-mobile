import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Config } from './config';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class GeolocationProvider {

  constructor(public http: HttpClient) {

  }

  getAddressDescripton(latitude: number, longitude: number): Promise<any> {

    return this.http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&sensor=true').toPromise();
  };

}
