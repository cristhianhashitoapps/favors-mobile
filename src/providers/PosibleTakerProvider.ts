import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { FavorService as FavorProider } from './FavorService';
import 'rxjs/add/operator/map';
import { Config } from './config';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PosibleTakerProvider {

  public urlBase: string = Config.ApiUrl;


  constructor(public http: HttpClient, private favorProvider: FavorProider) {

  }

  listByFavor(favorId: number): Promise<any> {
    return this.http.get([this.urlBase, 'fvposibletaker', favorId, 'getposibletakersbyfavor'].join('/')).toPromise();
  };

  public findById(id: number): Promise<any> {
    return this.http.get([this.urlBase, 'fvposibletaker', id].join('/')).toPromise();
  };

  update(posibleTaker: any): Observable<any> {
    return this.http.put([this.urlBase, 'fvposibletaker', posibleTaker.id].join('/'), posibleTaker);
  };

  //override function for notification
  updatePosibleTaker(posibleTaker: any): Observable<any> {
    return this.http.put([this.urlBase, 'fvposibletaker', posibleTaker.id,'update'].join('/'), posibleTaker);
  };

  getStatistics(id: number): Promise<any> {
    return this.http.get([this.urlBase, 'fvposibletaker', id, 'getaveragecalificationsbytakerid'].join('/')).toPromise();
  }

  getOpinions(id: number): Promise<any> {
    return this.http.get([this.urlBase, 'fvposibletaker', id, 'getopinionsbytakerid'].join('/')).toPromise();
  }

  confirmPostulant(postulantId: number): Promise<any> {
    console.log("acceptPostulant postulantId " + postulantId);
    var main = this;
    var favorProvider = this.favorProvider;
    return new Promise(
      function (resolve, reject) {
        console.log("Promise");
        main.findById(postulantId).then((response) => {
          let posibleTaker = response;
          posibleTaker.FvposibletakerstateId = 2;
          posibleTaker.efective = true;
          console.log(posibleTaker);
          main.updatePosibleTaker(posibleTaker).subscribe((response) => {
            console.log("Posible taker updated");
            console.log(response);
            favorProvider.findById(posibleTaker.FvfavorId).then((response) => {
              console.log(response);
              var favor = response;
              favor.FvfavorstateId = 2;
              favorProvider.update(favor).subscribe((response) => {
                console.log("El favor ha sido asignado exitosamente");
                resolve(response);
              });
            });
          });
        });
      }
    );
  }
}
