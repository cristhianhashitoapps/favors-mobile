import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { FavorDto } from '../dto/FavorDto';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Config } from './config';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class FavorService {

  public urlBase: string = Config.ApiUrl;

  constructor(public http: HttpClient) {
  }
  create(favorDto: FavorDto): Observable<any> {
    return this.http.post([this.urlBase, 'fvfavor'].join('/'), favorDto);
  };

  list(): Promise<any> {
    return this.http.get([this.urlBase, 'fvfavor'].join('/')).toPromise();
  };

  listUserFavors(userId: number): Promise<any> {
    return this.http.get([this.urlBase, 'fvfavor', userId, 'getfavorsbyuserid'].join('/')).toPromise();
  };

  findById(id: number): Promise<any> {
    return this.http.get([this.urlBase, 'fvfavor', id].join('/')).toPromise();
  };

  update(favor: any): Observable<any> {
    return this.http.put([this.urlBase, 'fvfavor', favor.id].join('/'), favor);
  };
}
