import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { CreditCardDto } from '../dto/CreditCardDto';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Config } from './config';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CreditCardProvider {

  public urlBase: string = Config.ApiUrl;

  constructor(public http: HttpClient) {
  }

  create(creditCardDto: CreditCardDto): Observable<any> {
    return this.http.post([this.urlBase, 'fvcreditcard', 'savecreditcardinpayu'].join('/'), creditCardDto);
  };

  delete(cc) {
    return this.http.post([this.urlBase, 'fvcreditcard', 'deletecreditcardinpayu'].join('/'), cc);//.map(res => res);
  }

  findByUserId(id: number): Promise<any> {
    return this.http.get([this.urlBase, 'fvcreditcard', id, 'getbyuser'].join('/')).toPromise();
  }

  PayWithPayU(transaction) {
    return this.http.post([this.urlBase, 'fvcreditcard', 'paywithpayu'].join('/'), transaction).map(res => res);
  }

  GetById(id) {
    return this.http.get([this.urlBase, 'fvcreditcard', id].join('/')).map(res => res);
  }

  GetCreditCards() {
    return this.http.get([this.urlBase, 'fvcreditcard'].join('/')).map(res => res);
  }
}
