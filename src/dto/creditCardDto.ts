class CreditCardDto {
    id: string;
    number: string;
    securitycode: Date;
    active: string;
    finaldate: string;
    FvbankId: number;
    FvuserId: number;
    token: string;
    FvuserName: string;
    FvbankName: string;
}

export { CreditCardDto };
