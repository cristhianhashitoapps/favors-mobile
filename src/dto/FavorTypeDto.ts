class FavorTypeDto {
    id: string;
    name: string;
    active: string;
    createdAt:Date;
    updatedAt:Date;
}

export { FavorTypeDto };
