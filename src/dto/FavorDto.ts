class FavorDto {
    id: string;
    description: string;
    active: string;
    durationhours: number;
    realdurationhours: number;
    price: number;
    createdAt: Date;
    updatedAt: Date;
    datefavor: Date;
    FvuserId: number;
    FvfavorstateId: number;
    FvtypeofcalificationId: number;
    FvaddressId: number;
    opinion: string;
}

export { FavorDto };
