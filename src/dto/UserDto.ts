class UserDto {
    id: string;
    email: string;
    firstname: string;
    lastname: string;
    typeofuser: number;
    statistics: any;
    opinions: any[];
    telefono: string;
    active: boolean;
}

export { UserDto };
