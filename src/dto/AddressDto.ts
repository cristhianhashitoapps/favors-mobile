class AddressDto {
    id: string;
    name: string;
    lat: number;
    lang: number;
    field2: string;
    field3: string;
    field4: string;
    complement: string;
    active: string;
    createdAt: Date;
    updatedAt: Date;
    FvuserId: number;
    FvaddressfieldId: number;
    FvcityId: number;
}

export { AddressDto };
