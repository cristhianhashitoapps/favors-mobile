import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthService } from 'ng2-ui-auth';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { CreditCardPage } from '../pages/credit-card/credit-card';
import { CreditCardDetailPage } from '../pages/credit-card/credit-card-detail/credit-card-detail';
import { CreditCardListPage } from '../pages/credit-card/credit-card-list/credit-card-list';
import { FavorPage } from '../pages/favor/favor';
import { SignInPage } from '../pages/sign-in/sign-in-page';
import { SignUpPage } from '../pages/sign-up-page/sign-up-page';
import { UserFactory } from '../providers/UserFactory';
import { SalvadiaHomePage } from '../pages/salvadia-home/salvadia-home';
import { SalvadiaTransaccionesPage } from '../pages/salvadia-transacciones/salvadia-transacciones'
import { ClienteTransaccionesPage } from '../pages/cliente-transacciones/cliente-transacciones';
import { App } from 'ionic-angular';
import * as rootScope from './rootScope';
import { IndexPage } from '../pages/index';
import { UserPage } from '../pages/user/user';
import { FCM } from '@ionic-native/fcm';
import { LocalNotifications } from '@ionic-native/local-notifications';

const $rootScope = rootScope.default;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  //pagina final
  rootPage: any = SignInPage;
  user: any;
  pages: Array<{ title: string, component: any, data: any }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public auth: AuthService,
    public events: Events,private fcm: FCM,private localNotifications: LocalNotifications,public userFactory:UserFactory) {
    this.initializeApp();

    events.subscribe('user:logged', (typeOfUser) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('Welcome', typeOfUser);
      if (typeOfUser === 0) {
        this.pages = [
          { title: 'Inicio', component: HomePage, data: null },
          { title: 'Mi cuenta', component: UserPage, data: null },
          { title: 'Tarjetas de crédito', component: CreditCardPage, data: null },
          { title: 'Transacciones', component: ClienteTransaccionesPage, data: null },
          { title: 'Cerrar Sesión', component: "Cerrar", data: null },
        ];
      } else if (typeOfUser === 1) {
        this.pages = [
          { title: 'Inicio', component: SalvadiaHomePage, data: null },
          { title: 'Mi cuenta', component: UserPage, data: null },
          { title: 'Transacciones', component: SalvadiaTransaccionesPage, data: null },
          { title: 'Cerrar Sesión', component: "Cerrar", data: null },
        ];
      } else {
        this.pages = [
          { title: 'Inicio', component: IndexPage, data: null },
          { title: 'Login', component: SignInPage, data: null },
          { title: 'Registro salvadia', component: SignUpPage, data: { salvadia: true } },
          { title: 'Registro usuario', component: SignUpPage, data: { salvadia: false } },
        ];
      }
    });

    // used for an example of ngFor and navigation

    //console.log(this.app);
    if (auth.isAuthenticated()) {
      this.user = $rootScope.user = auth.getPayload().sub;
    }
    console.log(auth.isAuthenticated());


    if (this.user !== undefined) {

      if (this.user.typeofuser == 0) {

        this.pages = [
          { title: 'Inicio', component: HomePage, data: null },
          { title: 'Mi cuenta', component: UserPage, data: null },
          { title: 'Tarjetas de crédito', component: CreditCardPage, data: null },
          { title: 'Transacciones', component: ClienteTransaccionesPage, data: null },
          { title: 'Cerrar Sesión', component: "Cerrar", data: null },
        ];

        this.rootPage = !auth.isAuthenticated() ? SignUpPage : HomePage;
      } else {
        this.pages = [
          { title: 'Inicio', component: SalvadiaHomePage, data: null },
          { title: 'Mi cuenta', component: UserPage, data: null },
          { title: 'Transacciones', component: SalvadiaTransaccionesPage, data: null },
          { title: 'Cerrar Sesión', component: "Cerrar", data: null },
        ];

        this.rootPage = !auth.isAuthenticated() ? SignUpPage : SalvadiaHomePage;
      }
    }
    else {
      this.pages = [
        { title: 'Inicio', component: IndexPage, data: null },
        { title: 'Login', component: SignInPage, data: null },
        { title: 'Registro salvadia', component: SignUpPage, data: { salvadia: true } },
        { title: 'Registro usuario', component: SignUpPage, data: { salvadia: false } }
      ];
      this.rootPage = IndexPage;
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      console.log("inicia gestion token--------------------------------------")
      console.log(this.fcm);
      this.fcm.getToken().then(x=>{
        console.log("token");
        console.log(x);
        $rootScope.tokenfcm = x;

        if (this.auth.isAuthenticated()) {
          console.log("esta autenticado cuando inicia gestion token //////______>>>")

          $rootScope.user.tokenfcm = x
                    
          this.userFactory
            .UpdateUser($rootScope.user.id,$rootScope.user)
            .subscribe(result => {
               console.log("se actualiza token fcm cuando esta autenticado---------->>>>>") 
            },err=>{
              console.log(err)
            });
        }

      })

      this.fcm.onNotification().subscribe(data => {
        var msje = JSON.parse(data.mensaje);        
        this.localNotifications.schedule({
          id: 1,
          text: msje.message,
          //sound: isAndroid? 'file://sound.mp3': 'file://beep.caf',
          //data: { secret: key }
        });
        if (data.wasTapped) {
          console.log('Received in background');
          console.log(data);
        } else {
          console.log('Received in foreground');
          console.log(data);
        }
      });

      this.fcm.onTokenRefresh().subscribe(token => {
        console.log("refresh");
        console.log(token);
        $rootScope.tokenfcm = token;
      });

    });
  }

  openPage(page) {
    if (page.component == 'Cerrar') {
      this.auth.logout().subscribe({
        error: (err: any) => alert(err.message),
        complete: () => {
          this.user = undefined
          $rootScope.user= undefined
          this.nav.setRoot(SignUpPage, {});
          this.events.publish('user:logged', 3);
        }
      });
      
    } else {
      this.nav.setRoot(page.component, page.data);
    }
  }

}
