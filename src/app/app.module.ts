import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule,Tabs } from 'ionic-angular';
import { Config } from '../providers/config';
import { CustomConfig } from 'ng2-ui-auth';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { CreditCardPage } from '../pages/credit-card/credit-card';
import { CreditCardDetailPage } from '../pages/credit-card/credit-card-detail/credit-card-detail';
import { CreditCardListPage } from '../pages/credit-card/credit-card-list/credit-card-list';
import { FavorPage } from '../pages/favor/favor';
import { FavorDetailPage } from '../pages/favor/favor-detail/favor-detail';
import { FavorListPage } from '../pages/favor/favor-list/favor-list';
import { FavorPostulantPage } from '../pages/favor/favor-postulant/favor-postulant';
import { ProfilePage } from '../pages/profile/profile';
import { Ng2UiAuthModule } from 'ng2-ui-auth';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SignUpPage } from '../pages/sign-up-page/sign-up-page';
import { SignInPage } from '../pages/sign-in/sign-in-page';
import { NewPasswordPage } from '../pages/new-password/new-password';
import { UserFactory } from '../providers/UserFactory';
import { BankProvider } from '../providers/BankProvider';
import { FavorTypeService } from '../providers/FavorTypeService';
import { FavorService } from '../providers/FavorService';
import { CreditCardProvider } from '../providers/CreditCardProvider';
import { GeolocationProvider } from '../providers/GeolocationProvider';
import { AddressFieldProvider } from '../providers/AddressFieldProvider';
import { AddressProvider } from '../providers/AddressProvider';
import { CityProvider } from '../providers/CityProvider';
import { PosibleTakerProvider } from '../providers/PosibleTakerProvider';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { MisionesAbiertasPage } from '../pages/misiones-abiertas/misiones-abiertas';
import { MisionesSalvadiaPage } from '../pages/misiones-salvadia/misiones-salvadia';
import { SalvadiaTransaccionesPage } from '../pages/salvadia-transacciones/salvadia-transacciones';
import { ClienteTransaccionesPage } from '../pages/cliente-transacciones/cliente-transacciones';
import { SalvadiaHomePage } from '../pages/salvadia-home/salvadia-home';
import { FavorStatePage } from '../pages/favor/favor-state/favor-state';
import { MissionProvider } from '../providers/mission/mission';
import { TypesProvider } from '../providers/types/types';
import { Geolocation } from '@ionic-native/geolocation';
import { TextMaskModule } from 'angular2-text-mask';
import { TooltipsModule } from 'ionic-tooltips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePipe } from "@angular/common";
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { TransactionProvider } from '../providers/transaction/transaction';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';

import { Ionic2RatingModule } from 'ionic2-rating';
import { IndexPage } from '../pages/index';
import { TransactionStateProvider } from '../providers/transaction-state/transaction-state';
import { UserPage } from '../pages/user/user';
import { TokenInterceptor } from '../providers/TokenInterceptor';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FCM } from '@ionic-native/fcm';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { FavorListModalPage } from '../pages/favor-list-modal/favor-list-modal';
import { CuponProvider } from '../providers/CuponProvider';
import {AutosizeModule} from 'ngx-autosize';

registerLocaleData(localeFr);


export class MyAuthConfig extends CustomConfig {
  loginUrl = [Config.ApiUrl, 'fvuser/login'].join('/');
  signupUrl = [Config.ApiUrl, 'fvuser/signup'].join('/');
  tokenName = 'Token';
  tokenPrefix = 'favors';

  defaultHeaders = { 'Content-Type': 'application/json' };
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CreditCardPage,
    CreditCardDetailPage,
    CreditCardListPage,
    FavorPage,
    FavorDetailPage,
    FavorListPage,
    FavorPostulantPage,
    ProfilePage,
    ListPage,
    SignUpPage,
    SignInPage,
    NewPasswordPage,
    MisionesAbiertasPage,
    MisionesSalvadiaPage,
    FavorStatePage,
    SalvadiaHomePage,
    IndexPage,
    SalvadiaTransaccionesPage,
    ClienteTransaccionesPage,
    UserPage,
    FavorListModalPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, { tabsPlacement: 'top' }),
    Ng2UiAuthModule.forRoot(MyAuthConfig),
    CurrencyMaskModule,
    TextMaskModule,
    TooltipsModule,
    BrowserAnimationsModule,
    Ionic2RatingModule,
    HttpClientModule,
    AutosizeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CreditCardPage,
    CreditCardDetailPage,
    CreditCardListPage,
    FavorPage,
    FavorDetailPage,
    FavorListPage,
    FavorPostulantPage,
    ProfilePage,
    ListPage,
    SignUpPage,
    SignInPage,
    NewPasswordPage,
    MisionesAbiertasPage,
    MisionesSalvadiaPage,
    FavorStatePage,
    SalvadiaHomePage,
    IndexPage,
    SalvadiaTransaccionesPage,
    ClienteTransaccionesPage,
    UserPage,
    FavorListModalPage
  ],
  providers: [

    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    UserFactory,
    FavorService,
    FavorTypeService,
    BankProvider,
    CreditCardProvider,
    MissionProvider,
    TypesProvider,
    GeolocationProvider,
    AddressFieldProvider,
    CityProvider,
    AddressProvider,
    PosibleTakerProvider,
    Geolocation,
    DatePipe,
    TransactionProvider,
    FileTransfer,
    FileTransferObject,
    File,
    Camera,
    TransactionStateProvider,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    FCM  ,
    LocalNotifications  ,
    Tabs,
    CuponProvider
    
  ]
})
export class AppModule { }
