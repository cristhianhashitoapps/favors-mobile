import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalvadiaHomePage } from './salvadia-home';

@NgModule({
  declarations: [
    SalvadiaHomePage,
  ],
  imports: [
    IonicPageModule.forChild(SalvadiaHomePage),
  ],
})
export class SalvadiaHomePageModule {}
