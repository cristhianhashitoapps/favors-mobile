import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { MisionesAbiertasPage } from '../misiones-abiertas/misiones-abiertas';
import { MisionesSalvadiaPage } from '../misiones-salvadia/misiones-salvadia';

/**
 * Generated class for the SalvadiaHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-salvadia-home',
  templateUrl: 'salvadia-home.html',
})
export class SalvadiaHomePage {

  @ViewChild(MisionesSalvadiaPage) misionesSalvadiaPage: MisionesSalvadiaPage;

  tab1Root = MisionesAbiertasPage;
  tab2Root = MisionesSalvadiaPage;
  myIndex: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.myIndex = navParams.data.tabIndex || 0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SalvadiaHomePage');

  }

  viewMyMisions() {
    console.log("SalvadiaHomePage viewMyMisions");
  }

  reloadMisions() {
    console.log("reloadMisions");
    this.navCtrl.setRoot(this.navCtrl.getActive().component, { tabIndex: 1 });
  }

}
