import { IonicPage, NavController, NavParams, MenuController, AlertController } from 'ionic-angular';
import { Component, Injector } from '@angular/core';
import { ViewController, Events, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UserFactory } from '../../providers/UserFactory';
import * as rootScope from '../../app/rootScope';
import { AuthService } from 'ng2-ui-auth';
import { ToastController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { SalvadiaHomePage } from '../salvadia-home/salvadia-home';
import { SignUpPage } from '../sign-up-page/sign-up-page';
import { Config } from '../../providers/config';

const $rootScope = rootScope.default;

@IonicPage()
@Component({
  selector: 'page-sign-in-page',
  templateUrl: 'sign-in-page.html'
})
export class SignInPage {

  public urlBase: string = Config.ApiUrl;
  public user;
  private loader: any;
  private loadingCtrl: LoadingController;


  constructor(
    private events: Events,
    private viewCtrl: ViewController,
    injector: Injector,
    public userFactory: UserFactory,
    public auth: AuthService,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    private alertController: AlertController
  ) {

    this.loadingCtrl = injector.get(LoadingController);
    this.user = {};

  }

  enableMenuSwipe() {
    return false;
  }

  ionViewDidLoad() {
  }

  onCancel() {
    //this.viewCtrl.dismiss();
    this.navCtrl.setRoot('SignUpPage');
  }

  SignIn() {
    const loadingText: String = "Espera un momento";
    this.loader = this.loadingCtrl.create({
      content: `<p class="item">${loadingText}</p>`,
    });
    this.loader.present();
    this.userFactory
      .Login(this.user)
      .subscribe(result => {
        console.log("Login sucessfull");
        console.log(result);
        this.auth.setToken(result["Token"]);
        $rootScope.user = this.auth.getPayload().sub;

        console.log("------------login-user-----------");
        console.log($rootScope.user);
        console.log("------------login-user-----------");

        //actualizar fcm en user
        if($rootScope.user.tokenfcm==null){
          console.log("no tiene token fcm"); 
          $rootScope.user.tokenfcm = $rootScope.tokenfcm;
          this.userFactory
            .UpdateUser($rootScope.user.id,$rootScope.user)
            .subscribe(result => {
               console.log("se actualiza token fcm---------->>>>>") 
            },err=>{
              console.log(err)
            });
            
        }
        
        if($rootScope.user.active==0){
            console.log("no entro por validacion");
            let modal = this.alertController.create({
              subTitle: 'Gracias por registrarte. Un agente se pondrá en contacto contigo para hacerte una entrevista y terminar el proceso',
              buttons: ['Terminar'],
              cssClass: "salvadiaConfirm"
            });
            modal.present();
            this.loader.dismiss();
        }else{
          $rootScope.user.imgUrl = this.urlBase + "/fvusers/" + $rootScope.user.id + "/img";
          this.userFactory.getImage($rootScope.user.id).then(response => {
  
          }, error => {
            $rootScope.user.imgUrl = "assets/imgs/logo-mini.png";
          });
  
          console.log($rootScope.user.imgUrl);
          if ($rootScope.user.typeofuser === 0) {
            this.navCtrl.setRoot(HomePage);
            this.events.publish('user:logged', 0);
          } else {
            this.navCtrl.setRoot(SalvadiaHomePage);
            this.events.publish('user:logged', 1);
          }
          this.loader.dismiss();

        }

       
      }, error => {
        console.log("------ERROR-----LOGIN");
        console.log(error);
        console.log("------ERROR-----LOGIN");

        this.toastCtrl.create({
          message: 'Atención: ' + error.error,
          duration: 2000,
          showCloseButton: true,
          closeButtonText: 'Ok'
        })
          .present();
        this.loader.dismiss();
      });
  }

  GoToSignUp() {
    const loadingText: String = "Espera un momento";
    this.loader = this.loadingCtrl.create({
      content: `<p class="item">${loadingText}</p>`,
    });
    this.navCtrl.setRoot(SignUpPage);
    this.loader.present();
    this.loader.dismiss();
  }

}
