import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisionesSalvadiaPage } from './misiones-salvadia';

@NgModule({
  declarations: [
    MisionesSalvadiaPage,
  ],
  imports: [
    IonicPageModule.forChild(MisionesSalvadiaPage),
  ],
})
export class MisionesSalvadiaPageModule {}
