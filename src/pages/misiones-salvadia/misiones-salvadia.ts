import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { MissionProvider } from '../../providers/mission/mission';
import { TypesProvider } from '../../providers/types/types';
import { UserFactory } from '../../providers/UserFactory';

import * as rootScope from '../../app/rootScope';
import { FavorStatePage } from '../favor/favor-state/favor-state';
const $rootScope = rootScope.default;

@IonicPage()
@Component({
	selector: 'page-misiones-salvadia',
	templateUrl: 'misiones-salvadia.html',
})
export class MisionesSalvadiaPage {

	@ViewChild(FavorStatePage) favorStatePage: FavorStatePage;

	private missionTypes;
	private missions = [];
	private types;
	private users;
	private loggedUser;
	public viewState: boolean = false;

	constructor(public navCtrl: NavController, public navParams: NavParams,
		public missionProvider: MissionProvider, public userFactory: UserFactory,
		public typesProvider: TypesProvider) {

		this.loggedUser = $rootScope.user;

		this.userFactory
			.GetUsers()
			.subscribe(
				res => {
					this.users = res["rows"]; //console.log(this.users);
				},
				err => { console.log('error: ', err); }
			);

		this.missionProvider
			.GetMissionsPossibleTakerBySalvadiaId(this.loggedUser.id)
			.subscribe(
				res=>{
					console.log("------------------xxxxxx-----------------------")
					console.log(res);
					console.log("------------------xxxxxx-----------------------")
					this.missions = res["rows"];
				},
				err => { console.log('error: ', err); }
			)		

		this.typesProvider
			.GetTypes()
			.subscribe(
				res => { this.types = res["rows"]; /*console.log(this.types);*/ },
				err => { console.log('error: ', err); }
			);

		this.typesProvider
			.GetMissionTypes()
			.subscribe(
				res => { this.missionTypes = res["rows"]; /*console.log(this.missionTypes);*/ },
				err => { console.log('error: ', err); }
			);


	}

	loadMisions() {
		console.log("inicia load missions");
		this.userFactory
			.GetUsers()
			.subscribe(
				res => {
					this.users = res["rows"]; //console.log(this.users);
				},
				err => { console.log('error: ', err); }
			);

			this.missionProvider
			.GetMissionsPossibleTakerBySalvadiaId(this.loggedUser.id)
			.subscribe(
				res=>{
					console.log(res);
					this.missions = res["rows"];
				},
				err => { console.log('error: ', err); }
			)	

			this.typesProvider
			.GetTypes()
			.subscribe(
				res => { this.types = res["rows"]; /*console.log(this.types);*/ },
				err => { console.log('error: ', err); }
			);

		this.typesProvider
			.GetMissionTypes()
			.subscribe(
				res => { this.missionTypes = res["rows"]; /*console.log(this.missionTypes);*/ },
				err => { console.log('error: ', err); }
			);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad MisionesSalvadiaPage');
	}

	estadoMision(favorId: number) {
		console.log("Mission Id: " + favorId);
		this.favorStatePage.loadState(favorId);
		this.viewState = true;
	}
	changeViewList() {
		this.viewState = false;
	}
}
