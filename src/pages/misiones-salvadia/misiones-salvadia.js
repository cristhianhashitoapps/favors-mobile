var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MissionProvider } from '../../providers/mission/mission';
import { TypesProvider } from '../../providers/types/types';
import { UserFactory } from '../../providers/UserFactory';
import * as rootScope from '../../app/rootScope';
import { FavorStatePage } from '../favor/favor-state/favor-state';
var $rootScope = rootScope.default;
var MisionesSalvadiaPage = /** @class */ (function () {
    function MisionesSalvadiaPage(navCtrl, navParams, missionProvider, userFactory, typesProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.missionProvider = missionProvider;
        this.userFactory = userFactory;
        this.typesProvider = typesProvider;
        this.missions = [];
        this.viewState = false;
        this.loggedUser = $rootScope.user;
        /*
                this.userFactory
                    .GetUsers()
                    .subscribe(
                        res => { this.users = res.rows; //console.log(this.users);
                        },
                        err => { console.log('error: ', err); }
                    );
        */
        /*
                this.missionProvider
                    .GetMissions()
                    .subscribe(
                        res => { this.missions = res.rows; //console.log(this.missions);
                        },
                        err => { console.log('error: ', err); }
                    );
        */
        this.missionProvider
            .GetMissionsPossibleTaker()
            .subscribe(function (res) {
            var salvadiaMissions = res.rows.filter(function (item) { return item.FvuserId == _this.loggedUser.id; });
            _this.missionProvider
                .GetMissions()
                .subscribe(function (res) {
                //this.missions = res.rows;
                Object.keys(salvadiaMissions).forEach(function (keyS) {
                    console.log(_this.missions);
                    Object.keys(res.rows).forEach(function (keyM) {
                        console.log(res.rows[keyM].id + " == " + salvadiaMissions[keyS].FvfavorId);
                        if (res.rows[keyM].id == salvadiaMissions[keyS].FvfavorId) {
                            _this.missions.push(res.rows[keyM]);
                        }
                    });
                });
            }, function (err) { console.log('error: ', err); });
        }, function (err) { console.log('error: ', err); });
        this.typesProvider
            .GetTypes()
            .subscribe(function (res) { _this.types = res.rows; /*console.log(this.types);*/ }, function (err) { console.log('error: ', err); });
        this.typesProvider
            .GetMissionTypes()
            .subscribe(function (res) { _this.missionTypes = res.rows; /*console.log(this.missionTypes);*/ }, function (err) { console.log('error: ', err); });
    }
    MisionesSalvadiaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MisionesSalvadiaPage');
    };
    MisionesSalvadiaPage.prototype.estadoMision = function (favorId) {
        console.log("Mission Id: " + favorId);
        this.favorStatePage.loadState(favorId);
        this.viewState = true;
    };
    __decorate([
        ViewChild(FavorStatePage),
        __metadata("design:type", FavorStatePage)
    ], MisionesSalvadiaPage.prototype, "favorStatePage", void 0);
    MisionesSalvadiaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-misiones-salvadia',
            templateUrl: 'misiones-salvadia.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams,
            MissionProvider, UserFactory,
            TypesProvider])
    ], MisionesSalvadiaPage);
    return MisionesSalvadiaPage;
}());
export { MisionesSalvadiaPage };
//# sourceMappingURL=misiones-salvadia.js.map