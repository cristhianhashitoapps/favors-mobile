import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

//import { SalvadiaHomePage } from '../salvadia-home/salvadia-home';
import { FavorPage } from '../favor/favor';

import * as rootScope from '../../app/rootScope';
const $rootScope = rootScope.default;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
    console.log("----------USER-----------------");
    console.log($rootScope.user);
    console.log("----------USER-----------------");
  	this.navCtrl.setRoot(FavorPage);
  }

}
