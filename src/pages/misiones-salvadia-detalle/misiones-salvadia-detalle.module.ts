import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisionesSalvadiaDetallePage } from './misiones-salvadia-detalle';

@NgModule({
  declarations: [
    MisionesSalvadiaDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(MisionesSalvadiaDetallePage),
  ],
})
export class MisionesSalvadiaDetallePageModule {}
