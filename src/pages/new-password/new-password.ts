import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { SignInPage } from '../sign-in/sign-in-page';
import { SignUpPage } from '../sign-up-page/sign-up-page';
import { UserFactory } from '../../providers/UserFactory';

@IonicPage()
@Component({
  selector: 'page-new-password',
  templateUrl: 'new-password.html',
})
export class NewPasswordPage {

  public email:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
      public userFactory:UserFactory,private alertController: AlertController,) {
  }
  
  recoverPassword(){
    this.userFactory
      .RecoverPassword(this.email)
      .subscribe(result=>{
        console.log(result)
        let modal = this.alertController.create({
          subTitle: 'Enviaremos instrucciones a su correo',
          buttons: ['Terminar'],
          cssClass: "salvadiaConfirm"
        });
        modal.present();
      },error=>{
        console.log(error)
        
      })
  }

}
