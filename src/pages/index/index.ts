import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignInPage } from '../sign-in/sign-in-page';
import { SignUpPage } from '../sign-up-page/sign-up-page';
import { NewPasswordPage } from '../new-password/new-password';

@IonicPage()
@Component({
  selector: 'page-index',
  templateUrl: 'index.html',
})
export class IndexPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  newSalvadia() {
    console.log("newSalvadia");
    this.navCtrl.setRoot(SignUpPage, { salvadia: true });
  }

  newUser() {
    console.log("newUser");
    this.navCtrl.setRoot(SignUpPage, { salvadia: false });
  }

  login() {
    console.log("login");
    this.navCtrl.setRoot(SignInPage);
  }

  newPassword(){
    console.log("login");
    this.navCtrl.setRoot(NewPasswordPage);
  }

}
