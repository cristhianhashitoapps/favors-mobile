import { Component, ViewChild, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams, Button, SegmentButton } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { FavorListPage } from './favor-list/favor-list';
import { FavorPostulantPage } from './favor-postulant/favor-postulant';
import { FavorStatePage } from './favor-state/favor-state';
import * as rootScope from '../../app/rootScope';
const $rootScope = rootScope.default;
import { Config } from '../../providers/config';
import { UserFactory } from '../../providers/UserFactory';
import { FavorDetailPage } from './favor-detail/favor-detail';

@IonicPage()
@Component({
  selector: 'page-favor',
  templateUrl: 'favor.html',
})
export class FavorPage {

  urlBase: string = Config.ApiUrl;

  @ViewChild(ProfilePage) profilePage: ProfilePage;
  @ViewChild(FavorListPage) favorListPage: FavorListPage;
  @ViewChild(FavorPostulantPage) favorPostulantPage: FavorPostulantPage;
  @ViewChild(FavorStatePage) favorStatePage: FavorStatePage;
  @ViewChild(FavorDetailPage) FavorDetailPage: FavorDetailPage;

  @ViewChild("changeToList") changeToList: SegmentButton;

  loggedUser: any = { 'firstname': '', 'lastname': '' };
  tab: string = 'create';
  viewList: boolean = true;
  viewPostulants: boolean = false;
  viewProfile: boolean = false;
  viewState: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private userFactory: UserFactory,private renderer:Renderer) {
    this.loggedUser = $rootScope.user;
    this.loggedUser.imgUrl = this.urlBase + "/fvusers/" + this.loggedUser.id + "/img";
    this.userFactory.getImage(this.loggedUser.id).then(response => {

    }, error => {
      this.loggedUser.imgUrl = "assets/imgs/logo-mini.png";
    });
  }


  changeViewPostulants(favorId: number) {
    console.log("View Postulants favorId: " + favorId);
    this.favorPostulantPage.listFavorPostulant(favorId);
    this.viewList = false;
    this.viewPostulants = true;
    this.viewProfile = false;
    this.viewState = false;
  }

  changeViewProfile(postulant: any) {
    console.log("View Profile: " + postulant.FvuserId.toString());
    this.profilePage.viewProfile(postulant);
    this.viewList = false;
    this.viewPostulants = false;
    this.viewProfile = true;
    this.viewState = false;
  }

  changeViewList() {
    console.log("ChangeViewLis---");
    if (this.favorListPage) {
      this.favorListPage.listUserFavors($rootScope.user.id);
    }
    this.viewList = true;
    this.viewPostulants = false;
    this.viewProfile = false;
    this.viewState = false;
  }

  changeViewState(favorId: number) {
    console.log("ChangeViewState");
    this.favorStatePage.loadState(favorId);
    this.viewList = false;
    this.viewPostulants = false;
    this.viewProfile = false;
    this.viewState = true;
  }

  viewAlgoEvent(){
    console.log("algo")    
    this.changeToList.onClick()
  }
}
