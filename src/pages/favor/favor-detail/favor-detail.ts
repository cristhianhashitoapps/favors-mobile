import { Component, ViewChild, ElementRef } from '@angular/core';
import { AlertController, Nav, SegmentButton, IonicFormInput } from 'ionic-angular';
import { FavorDto } from '../../../dto/FavorDto';
import * as rootScope from '../../../app/rootScope';
import { AddressDto } from '../../../dto/AddressDto';
import { Geolocation } from '@ionic-native/geolocation';
import { FavorService } from '../../../providers/FavorService';
import { CityProvider } from '../../../providers/CityProvider';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { IonicPage, NavController, NavParams,Tabs } from 'ionic-angular';
import { MissionProvider } from '../../../providers/mission/mission';
import { AddressProvider } from '../../../providers/AddressProvider';
import { FavorTypeService } from '../../../providers/FavorTypeService';
import { GeolocationProvider } from '../../../providers/GeolocationProvider';
import { AddressFieldProvider } from '../../../providers/AddressFieldProvider';
import { CreditCardProvider } from '../../../providers/CreditCardProvider';
import { FavorListModalPage } from '../../favor-list-modal/favor-list-modal';
import { ModalController } from 'ionic-angular';
import { Output, EventEmitter } from '@angular/core';
import { CuponProvider } from '../../../providers/CuponProvider';


var $rootScope = rootScope.default;

@IonicPage()
@Component({
  selector: 'page-favor-detail',
  templateUrl: 'favor-detail.html',
})

export class FavorDetailPage {

  @Output() viewListEvent: EventEmitter<number> = new EventEmitter();
  @ViewChild("hours") hours:any;

  favorGroup: FormGroup;
  lstFavorTypes: any = { 'rows': null };
  lstAddressType: any = { 'rows': null };
  lstCities: any = { 'rows': null };
  gps: any = { latitude: null, longitude: null };
  hideRegisterCreditoFor: boolean=false;
  cupon: String;
  cuponApplied: Boolean;
  cuponAppliedResult: String;
  possibleCuponResult1: String ="Se aplicó correctamente el cupón, puedes disfrutar de tu favor de 1 hora.";
  possibleCuponResult2: String ="Lo sentimos no se aplicó el cupón, debes ingresar una tarjeta de crédito";
  possibleCuponResult3: String ="El cupon ya lo usaste una vez, debes ingresar una tarjeta de crédito";
  
  hideCuponForm:Boolean;

  constructor(
    private formBuilder: FormBuilder, private favorTypeService: FavorTypeService,
    private favorService: FavorService, private alertCtrl: AlertController,
    private geolocation: Geolocation, private geolocationProvider: GeolocationProvider,
    private addressFieldProvider: AddressFieldProvider, private cityProvider: CityProvider,
    private addressProvider: AddressProvider, private missionProvider: MissionProvider,
    private creditCardProvider: CreditCardProvider,public modalController: ModalController, public nav:Nav,
    private cuponProvider: CuponProvider) {

      this.cuponApplied=false;
      this.hideCuponForm=true;


    this.favorGroup = this.formBuilder.group({
      categoryId: ['', Validators.required],
      description: ['', Validators.required],
      cityId: ['', Validators.required],
      addressPrincipalType: ['', Validators.required],
      addressPrincipal: ['', Validators.required],
      addressDefinition: ['', Validators.required],
      addressDefinitionComplement: ['', Validators.required],
      addresComplement: [''],
      date: ['', Validators.required],
      hour: ['', Validators.required],
      price: ['10000', Validators.required],
      hours: ['0', Validators.required]
    });

    
    

    this.favorTypeService.list().then(res => {
      this.lstFavorTypes = res;
    });

    this.cityProvider.list().then(res => {
      this.lstCities = res;
      console.log(this.lstCities);
    });

    this.addressFieldProvider.list().then(res => {
      this.lstAddressType = res;
    });

    this.geolocation.getCurrentPosition().then((resp) => {
      console.log("Current geolocation latitude " + resp.coords.latitude + " longitude " + resp.coords.longitude);
      this.gps.latitude = resp.coords.latitude;
      this.gps.longitude = resp.coords.longitude;

      geolocationProvider
        .getAddressDescripton(resp.coords.latitude, resp.coords.longitude)
        .then((descResponse) => {
          var addressPrincipal = descResponse.results[0].address_components[1].long_name;
          var addressSplited = addressPrincipal.split(" ");

          for (let addressType of this.lstAddressType.rows) {
            if (addressType.name == addressSplited[0]) {
              this.favorGroup.controls['addressPrincipalType'].setValue(addressType.id);
              break;
            }
          }

          for (let city of this.lstCities.rows) {
            var cityGPS = descResponse.results[0].address_components[4].long_name;
            cityGPS = this.removeSpecialCharacters(cityGPS);
            if (city.name == cityGPS.toUpperCase()) {
              this.favorGroup.controls['cityId'].setValue(city.id);
              break;
            }
          }

          this.favorGroup.controls['addressPrincipal'].setValue(addressSplited[1]);
        })
        .catch((error) => {
          console.log('Error getting location', error);
        });
    })
      .catch((error) => {
        console.log('Error getting location', error);
      });

      //consultar tarjetas de credito
      this.creditCardProvider
        .findByUserId($rootScope.user.id)
        .then(res=>{
          if(res.rows.length>0){
            console.log("entra aqui--------");
            this.hideRegisterCreditoFor=false;
          }else{
            this.hideRegisterCreditoFor=true;
          }
        })
        .catch(err=>{
          console.log(err);
        })
  }

  removeSpecialCharacters(s) {
    var r = s.toLowerCase();
    r = r.replace(new RegExp(/\s/g), "");
    r = r.replace(new RegExp(/[àáâãäå]/g), "a");
    r = r.replace(new RegExp(/[èéêë]/g), "e");
    r = r.replace(new RegExp(/[ìíîï]/g), "i");
    r = r.replace(new RegExp(/ñ/g), "n");
    r = r.replace(new RegExp(/[òóôõö]/g), "o");
    r = r.replace(new RegExp(/[ùúûü]/g), "u");

    return r;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavorDetailPage');
  }

  public openModal(id){
      //abrir modal lista favores
      /*const modal= await this.modalController.create({
        component : FavorListModalPage
      })

      return await modal.present()*/
      //this.nav.insert(1,FavorListModalPage  )
      console.log("jajaj")
      //console.log(this.viewListEvent)

  }

  public guardar(form): void {
    if (this.favorGroup.value.price >= 10000 && this.favorGroup.value.hours>0) {
      console.log("Almacenando dirección");
      var address: AddressDto = {
        id: null,
        name: '',
        lat: this.gps.latitude,
        lang: this.gps.longitude,
        field2: this.favorGroup.value.addressPrincipal,
        field3: this.favorGroup.value.addressDefinition,
        field4: this.favorGroup.value.addressDefinitionComplement,
        complement: this.favorGroup.value.addresComplement,
        active: '1',
        createdAt: new Date(),
        updatedAt: new Date(),
        FvuserId: $rootScope.user.id,
        FvaddressfieldId: this.favorGroup.value.addressPrincipalType,
        FvcityId: this.favorGroup.value.cityId,
      };


      this.addressProvider.create(address).subscribe(
        res => {
          console.log("creando bien favor----");
          let date = new Date(this.favorGroup.value.date + "T" + this.favorGroup.value.hour);
          //para que no envie + 5 horas          
          date.setMinutes(date.getMinutes()-300);
          //para que no envie + 5 horas                    
          console.log("creando date favor----"),date;
          console.log(this.favorGroup.value);
          console.log($rootScope.user);

          let creationDate = new Date();
          creationDate.setMinutes(date.getMinutes()-300);

          try{
            var favor: FavorDto = {
              id: null,
              description: this.favorGroup.value.description,
              active: "1",
              durationhours: this.favorGroup.value.hours,
              realdurationhours: 0,
              price: this.favorGroup.value.price,
              createdAt: creationDate,
              updatedAt: creationDate,
              //datefavor: new Date(date.toISOString()),//android
              datefavor: date,
              FvuserId: $rootScope.user.id,
              FvfavorstateId: 1,
              FvaddressId: res.id,
              FvtypeofcalificationId: 5,
              opinion: ""
            };
          }catch(error){
            console.log("error::::::",error);
          }


          this.favorService.create(favor).subscribe(
            res => {

              var data = {
                id: null,
                createdAt: favor.createdAt,
                updatedAt: favor.updatedAt,
                FvfavorId: res.id,
                FvfavortypeId: this.favorGroup.value.categoryId,
              };

              this.missionProvider
                .AddTypesToMission(data)
                .subscribe(
                  res => {


                    let alert = this.alertCtrl.create({
                      title: 'Operación exitosa',
                      subTitle: 'Su favor ha sido creada exitosamente',
                      buttons: ['Aceptar']
                    });
                    alert.present(); 
                    this.favorGroup.reset();


                    this.viewListEvent.emit()
                    //console.log(this.viewPostulantsEvent)
                    //this.openModal(res.id);

                    
                    
                  },
                  err => { console.log('error: ', err); }
                );
            }
          );
        }
      );
    }
    else {
      let alert = this.alertCtrl.create({
        title: 'Verificar valor por hora',
        subTitle: 'El valor mínimo aceptado es COP 10.000 y la cantidad de horas mayor a 0',
        buttons: ['Aceptar']
      });
      alert.present();
    }
  }

  public iHaveCupon():void{

    this.cuponProvider
    .GetByName(this.cupon)
    .then((res:any)=>{
      console.log(res)
      
      if(res.count>0){
        $rootScope.cupon = res.rows[0].name;
        this.cuponProvider
        .GetByUserAndCupon(this.cupon,$rootScope.user.id)
        .then((res:any)=>{
         if(res.count>0){
          this.cuponAppliedResult = this.possibleCuponResult3;
          this.hideCuponForm=false;
          this.cuponApplied=true;
          //this.hideRegisterCreditoFor=true;
          $rootScope.cupon = ""
         }else{
           //CUPON VALIDO           
              this.cuponAppliedResult = this.possibleCuponResult1;
              this.hideCuponForm=false;
              this.cuponApplied=true;
              this.hideRegisterCreditoFor=false;
              //lo graba para poder recuperarlo mas adelante
              //y valida si tiene cupon
              
              this.favorGroup.value.hours = 1;
              console.log(this.hours);
              this.hours.value=1;
              //this.hours.disabled=true; 
            
         }
        })
        .catch(err=>{
          console.log(err)
        })
      }else{
            this.cuponAppliedResult = this.possibleCuponResult2;
            this.hideCuponForm=false;
            this.cuponApplied=true;
            this.hideRegisterCreditoFor=true;
            $rootScope.cupon = ""
          }  

      console.log("user-----"+$rootScope.user.id)

     
      
    })
    .catch(err=>{
      console.log(err);
    })
    
  }
}
