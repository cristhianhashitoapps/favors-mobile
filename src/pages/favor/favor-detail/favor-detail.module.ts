import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavorDetailPage } from './favor-detail';


@NgModule({
  declarations: [
    FavorDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(FavorDetailPage)
    
  ],
})
export class FavorDetailPageModule {}
