import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavorStatePage } from './favor-state';

@NgModule({
  declarations: [
    FavorStatePage,
  ],
  imports: [
    IonicPageModule.forChild(FavorStatePage),
  ],
})
export class FavorStatePageModule {}
