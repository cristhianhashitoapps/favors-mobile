import { Component } from '@angular/core';
import { FavorDto } from '../../../dto/FavorDto';
import { Ionic2RatingModule } from 'ionic2-rating';
import * as rootScope from '../../../app/rootScope';
import { Loading, LoadingController } from 'ionic-angular';
import { CreditCardDto } from '../../../dto/creditCardDto';
import { UserFactory } from '../../../providers/UserFactory';
import { AddressProvider } from '../../../providers/AddressProvider';
import { MissionProvider } from '../../../providers/mission/mission';
import { CreditCardProvider } from '../../../providers/CreditCardProvider';
import { FavorService as FavorProvider } from '../../../providers/FavorService';
import { TransactionProvider } from '../../../providers/transaction/transaction';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { CuponProvider } from '../../../providers/CuponProvider';
import { DomSanitizer } from '@angular/platform-browser';
import { Config } from '../../../providers/config';

const $rootScope = rootScope.default;


@IonicPage()
@Component({
  selector: 'page-favor-state',
  templateUrl: 'favor-state.html',
})
export class FavorStatePage {

  favor: FavorDto = new FavorDto();
  loggedUser: any = { 'firstname': '', 'lastname': '' };
  salvadia: any = { 'firstname': '', 'lastname': '' };
  address: any = {};
  rate: any;
  opinion: any;
  favoruser: any = {'firstname':'','lastname':''};
  pathImageSalvadia:any;
  public urlImg: string = Config.ApiImg;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private favorProvider: FavorProvider, private alertController: AlertController,
    private transactionProvider: TransactionProvider, private creditCardProvider: CreditCardProvider,
    private userProvider: UserFactory, private missionProvider: MissionProvider,
    private addressProvider: AddressProvider, public loadingCtrl: LoadingController, public cuponProvider:CuponProvider,private sanitizer:DomSanitizer) {
    console.log('FavorStatePage constructor');
    this.loggedUser = $rootScope.user;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavorStatePage');
  }

  onSaveRateOpinion() {
    //onModelChange(event) {
      this.favor.FvtypeofcalificationId = this.rate;
      this.favor.opinion = this.opinion;

      this.missionProvider.updateMission(this.favor)
      .subscribe(
        res => { console.log(res); },
        err => { console.log(err); }
        );
    }

    loadState(favorId: number) {
      console.log('FavorStatePage loadState ' + favorId);
      this.favorProvider.findById(favorId).then((response) => {
        this.favor = response;

        console.log('this.favor', this.favor);

        this.addressProvider.findById(this.favor.FvaddressId).subscribe((response) => {
          this.address = response;
        });

        console.log('Mission: ', this.favor);

        this.rate = this.favor.FvtypeofcalificationId;
        this.opinion = this.favor.opinion;

        this.missionProvider
        .GetMissionsPossibleTaker()
        .subscribe(
          res => {
            
            var salvadiaMissions = res['rows'].filter(item => (item.FvfavorId == this.favor.id && (item.FvposibletakerstateId == 1 || item.FvposibletakerstateId == 2)));

            console.log(salvadiaMissions[0]);  

            this.userProvider
            .GetUser(salvadiaMissions[0].FvuserId)
            .subscribe(
              res => {
                console.log('salvadia', res);
                this.salvadia = res;
                this.pathImageSalvadia = this.sanitizer.bypassSecurityTrustUrl([this.urlImg,this.salvadia.img].join('/'));

                //consultar el usuario dueno del favor
                this.userProvider
                .GetUser(this.favor.FvuserId)
                .subscribe(
                  res => {
                    console.log("favoruser:-------------------");                    
                    this.favoruser = res;
                    console.log(this.favoruser);
                  },
                  err => {
                    console.log('Error: ', err);
                  }
                  );

              },
              err => {
                console.log('Error: ', err);
              }
              );
          },
          err => { console.log('error: ', err); }
          );

      });
    }

    notifyMisionStart(favorId: number) {
      console.log('FavorStatePage notifyMisionStart ' + favorId);
      this.favorProvider.findById(favorId).then((response) => {
        let favor: FavorDto = response;
        favor.FvfavorstateId = 3;
        this.favorProvider.update(favor).subscribe((response) => {
          let alert = this.alertController.create({
            title: 'Operación exitosa',
            subTitle: 'Se ha iniciado la misión',
            buttons: ['Aceptar']
          });
          alert.present();
          this.loadState(favorId);
          console.log('Se ha confirmado que la misión se ha iniciado');
        });
      });
    }

    notifyMisionFinished(favorId: number) {
      console.log('FavorStatePage notifyMisionFinished ' + favorId);
      this.favorProvider.findById(favorId).then((response) => {
        let favor: FavorDto = response;
        favor.FvfavorstateId = 4;
        this.favorProvider.update(favor).subscribe((response) => {
          let alert = this.alertController.create({
            title: 'Operación exitosa',
            subTitle: 'Se ha terminado la misión, es necesario esperar la confirmación del cliente',
            buttons: ['Aceptar']
          });
          alert.present();
          this.loadState(favorId);
          console.log('Se ha notificado que la misión se ha completado');
        });
      });
    }

    confirmMisionFinished(favorId: number) {

      let loadControl = this.loadingCtrl.create({
        spinner: 'hide',
        content: "Procesando pago, por favor espere un momento..."
      });

      loadControl.present();

      var cuponDef:any

      this.transactionProvider.GetTransactionsByFavorId(favorId).subscribe((transactions:any)=>{
        if(transactions.count==0){
            cuponDef = $rootScope.cupon
        }else{
            cuponDef = transactions.rows[transactions.count-1].cupon
        }
        this.initPayment(cuponDef,favorId,loadControl)

      },error=>{
        console.log(error)
      })    

     
    }

    initPayment(cuponName:any,favorId:any,loadControl){
      
      this.favorProvider.findById(favorId).then((response) => {
        let favor: FavorDto = response;
       
        //calcular durationshours x price del favor = total favor
        var totalFavor = favor.durationhours * favor.price;


        if(cuponName == undefined || cuponName==""){
          console.log("ONLY CREDITCARD--->")
          this.payWithCreditCard(favorId,loadControl,totalFavor,cuponName);
        }else{
          console.log("averiguando cupon"+cuponName)
          this.cuponProvider.GetByName(cuponName)
            .then(cuponRes=>{
            //si el total favor <= valor cupon
            var cuponResVector:any = cuponRes
            var cupon:any = cuponResVector.rows[0]
            console.log("-----------cupon------------")
            console.log("cupon "+cupon)
            console.log("-----------cupon------------")
            if(totalFavor<=cupon.valuedolar){
              //entonces pagar con cupon
              console.log("ONLY CUPON2--->")
              this.payWithCupon(favorId,loadControl,cuponName)
            }
            //sino total favor > valor cupon
            else{
              //entonces calcular diferencia = total favor - valor cupon
              console.log("CREDITCARD - DIFERENCE--->")
              var diference = totalFavor - cupon.valuedolar
              console.log("-----------diference------------")
              console.log("totalFavor "+totalFavor)
              console.log("cupon.valuedolar "+cupon.valuedolar)
              console.log(diference)
              console.log("-----------diference------------")
              this.payWithCreditCard(favorId,loadControl,diference,cuponName)
              
            }
          })
        }          

      });       
    }


    payWithCupon(favorId:number,loadControl:any,cuponName){
     
        var data = {
          payujsonresponse: '',
          payucode: '',
          paytransactionid: '',
          payustate: '',
          //createdAt : new Date().toLocaleDateString(),
          //updatedAt : new Date().toLocaleDateString(),
          FvcreditcardId: null,
          FvfavorId: favorId,
          FvtransactionstateId: 1,
          cupon: cuponName,

        };

        this.transactionProvider.CreateTransaction(data)
        .subscribe(
          res => {
            this.favorProvider.findById(favorId).then((response) => {

              let favor: FavorDto = response;
              favor.FvfavorstateId = 5;

              this.favorProvider.update(favor).subscribe(

                res => {

                  this.cuponProvider.
                    CreateCuponForUser($rootScope.cupon,favor.FvuserId).then(res=>{
                      let alert = this.alertController.create({
                        title: 'Pago exitoso',
                        subTitle: 'La transacción fue realizada con exito.',
                        buttons: ['Aceptar']
                      });
                      alert.present();
    
                      this.loadState(favorId);
    
                      console.log('Se ha confirmado que la misión se ha completado');
    
                      loadControl.dismiss();
                    })
                    .catch(err=>{
                      console.log('Error pagando con cupon y confirmando');

                    })

                },
                err => {

                  let alert = this.alertController.create({
                    title: 'Error al pagar',
                    subTitle: 'Se presentó el siguiente error: ' + err,
                    buttons: ['Aceptar']
                  });

                  alert.present();
                  console.log('error: ', err);

                  loadControl.dismiss();
                });
            });
          },
          err =>{
            console.log(err);
          }
        )                        
    }



    payWithCreditCard(favorId:number,loadControl:any,valuedolar:any,cuponName){
      this.creditCardProvider
      .findByUserId(this.loggedUser.id).then((response) => {

        console.log('FavorState - Read CC');
        console.log(response);

        let cc: CreditCardDto = response.rows[0];

        var data = {
          payujsonresponse: '',
          payucode: '',
          paytransactionid: '',
          payustate: '',
          //createdAt : new Date().toLocaleDateString(),
          //updatedAt : new Date().toLocaleDateString(),
          FvcreditcardId: cc.id,
          FvfavorId: favorId,
          FvtransactionstateId: 1,
          cupon: cuponName
        };

        this.transactionProvider.CreateTransaction(data)
        .subscribe(
          res => {
            console.log('FavorState - CreateTransaction');
            console.log(res);

            this.transactionProvider.GetTransaction(res['id'])
            .subscribe(
              res => {
                console.log('FavorState - GetTransaction');
                console.log(res);

                var transaction:any = res['rows'][0]
                transaction.valuedolar = valuedolar

                this.creditCardProvider.PayWithPayU(transaction)
                .subscribe(
                  res => {
                    console.log('FavorState - PayWithPayU');
                    console.log(res);

                          /*
                          JKVP. 2018-09-08
                          */

                          //if(res['payucode'] == 'SUCCESS' && res['APPROVED'] == ''){
                          if(res['payucode'] == 'SUCCESS' && res['payustate'] == 'APPROVED'){

                            console.log('FavorStatePage confirmMisionFinished ' + favorId);

                            this.favorProvider.findById(favorId).then((response) => {

                              let favor: FavorDto = response;
                              favor.FvfavorstateId = 5;

                              this.favorProvider.update(favor).subscribe(

                                res => {

                                  let alert = this.alertController.create({
                                    title: 'Pago exitoso',
                                    subTitle: 'La transacción fue realizada con exito.',
                                    buttons: ['Aceptar']
                                  });
                                  alert.present();

                                  this.loadState(favorId);

                                  console.log('Se ha confirmado que la misión se ha completado');

                                  loadControl.dismiss();
                                },
                                err => {

                                  let alert = this.alertController.create({
                                    title: 'Error al pagar',
                                    subTitle: 'Se presentó el siguiente error: ' + err,
                                    buttons: ['Aceptar']
                                  });

                                  alert.present();
                                  console.log('error: ', err);

                                  loadControl.dismiss();
                                });
                            });
                          }
                          else{

                            let alert = this.alertController.create({
                              title: 'Error al finalizar y pagar',
                              subTitle: 'Por favor intenta mas tarde finalizar y pagar tu misión.',
                              buttons: ['Aceptar']
                            });

                            alert.present();
                            console.log("favor-state - confirmMisionFinished - Entro else de if(res['payucode'] == 'SUCCESS' && res['APPROVED'] == '')");

                            loadControl.dismiss();
                          }
                        },
                        err => {

                          let alert = this.alertController.create({
                            title: 'Error al pagar',
                            subTitle: 'Se presentó el siguiente error: ' + err,
                            buttons: ['Aceptar']
                          });

                          alert.present();
                          console.log('error: ', err);

                          loadControl.dismiss();
                        });
              },
              err => {

                let alert = this.alertController.create({
                  title: 'Error al pagar',
                  subTitle: 'Se presentó el siguiente error: ' + err,
                  buttons: ['Aceptar']
                });

                alert.present();
                console.log('error: ', err);

                loadControl.dismiss();
              });
          },
          err => {

            let alert = this.alertController.create({
              title: 'Error al pagar',
              subTitle: 'Se presentó el siguiente error: ' + err,
              buttons: ['Aceptar']
            });

            alert.present();
            console.log('error: ', err);

            loadControl.dismiss();
          });
        });
    }
}
