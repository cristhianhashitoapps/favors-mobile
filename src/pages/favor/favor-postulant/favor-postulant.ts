import { Component, Output, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { PosibleTakerProvider } from '../../../providers/PosibleTakerProvider';
import { Config } from '../../../providers/config';

@IonicPage()
@Component({
  selector: 'page-favor-postulant',
  templateUrl: 'favor-postulant.html',
})
export class FavorPostulantPage {

  @Output() viewProfileEvent: EventEmitter<any> = new EventEmitter();
  @Output() viewListEvent: EventEmitter<any> = new EventEmitter();
  lstPostulants: any = { 'rows': null };
  urlBase: string = Config.ApiUrl;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private posibleTakerProvider: PosibleTakerProvider, private alertController: AlertController) {
  }

  changeViewProfile(postulant: any) {
    console.log("changeViewProfile: favorId " + postulant.FvfavorId.toString());
    this.viewProfileEvent.emit(postulant);
  }

  listFavorPostulant(favorId: number) {
    console.log("FavorPostulantPage listFavorPostulant favorId: " + favorId);
    this.posibleTakerProvider.listByFavor(favorId).then(res => {
      console.log(res);
      this.lstPostulants = res;
      for (let postulant of this.lstPostulants.rows) {
        this.posibleTakerProvider.getStatistics(postulant.Fvuser.id).then((response) => {
          postulant.statistics = response;
        });
      }
    });
  }

  acceptPostulant(postulantId: number) {
    console.log("acceptPostulant postulantId " + postulantId);
    this.posibleTakerProvider.confirmPostulant(postulantId).then((response) => {
      console.log("Confirm ok");
      this.viewListEvent.emit();
      let alert = this.alertController.create({
        title: 'Operación exitosa',
        subTitle: 'El postulante ha sido aceptado',
        buttons: ['Aceptar']
      });
      alert.present();
    });
  }

}
