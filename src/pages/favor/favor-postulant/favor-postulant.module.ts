import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavorPostulantPage } from './favor-postulant';

@NgModule({
  declarations: [
    FavorPostulantPage,
  ],
  imports: [
    IonicPageModule.forChild(FavorPostulantPage),
  ],
})
export class FavorPostulantPageModule {}
