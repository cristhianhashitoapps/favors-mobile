import { Component, Output, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FavorService } from '../../../providers/FavorService';
import * as rootScope from '../../../app/rootScope';
const $rootScope = rootScope.default;

@IonicPage()
@Component({
  selector: 'page-favor-list',
  templateUrl: 'favor-list.html',
})
export class FavorListPage {

  @Output() viewPostulantsEvent: EventEmitter<number> = new EventEmitter();
  @Output() viewStateEvent: EventEmitter<number> = new EventEmitter();
  lstFavors: any = { 'rows': null };

  constructor(public navCtrl: NavController, public navParams: NavParams, private favorService: FavorService) {
    console.log('FavorListPage constructor');
    this.listUserFavors($rootScope.user.id);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavorListPage');
  }

  viewPostulants(favorId: number) {
    console.log('FavorListPage viewPostulants favorId: ' + favorId);
    this.viewPostulantsEvent.emit(favorId);
  }

  viewState(favorId: number) {
    console.log('FavorListPage viewState favorId: ' + favorId);
    this.viewStateEvent.emit(favorId);
  }

  listUserFavors(userId: number) {
    console.log("FavorListPage listUserFavors userId: " + userId);
    this.favorService.listUserFavors(userId).then(res => {
      this.lstFavors = res;
    });
  }

}
