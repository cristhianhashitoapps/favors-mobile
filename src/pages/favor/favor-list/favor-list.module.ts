import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavorListPage } from './favor-list';

@NgModule({
  declarations: [
    FavorListPage,
  ],
  imports: [
    IonicPageModule.forChild(FavorListPage)    
  ],
})
export class FavorListPageModule {}
