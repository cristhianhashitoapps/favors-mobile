import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalvadiaTransaccionesPage } from './salvadia-transacciones';

@NgModule({
  declarations: [
    SalvadiaTransaccionesPage,
  ],
  imports: [
    IonicPageModule.forChild(SalvadiaTransaccionesPage),
  ],
})
export class SalvadiaTransaccionesPageModule {}
