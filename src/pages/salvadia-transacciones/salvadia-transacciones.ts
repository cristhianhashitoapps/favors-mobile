import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TransactionProvider } from '../../providers/transaction/transaction';
import { MissionProvider } from '../../providers/mission/mission';
import { CreditCardProvider } from '../../providers/CreditCardProvider';
import { UserFactory } from '../../providers/UserFactory';
import { TransactionStateProvider } from '../../providers/transaction-state/transaction-state';

import * as rootScope from '../../app/rootScope';
const $rootScope = rootScope.default;
import { Config } from '../../providers/config';

@IonicPage()
@Component({
	selector: 'page-salvadia-transacciones',
	templateUrl: 'salvadia-transacciones.html',
})
export class SalvadiaTransaccionesPage {

	urlBase: string = Config.ApiUrl;

	private loggedUser;
	private users;
	private missions;
	private salvadiaMissions;
	private salvadiaTransactions;
	private CreditCards;
	private transactionStates;

	constructor(public navCtrl: NavController, public navParams: NavParams,
		public missionProvider: MissionProvider, public creditCardProvider: CreditCardProvider,
		public transactionStateProvider: TransactionStateProvider, public transactionProvider: TransactionProvider,
		public userFactory: UserFactory) {
		this.loggedUser = $rootScope.user;
		this.loggedUser.imgUrl = this.urlBase + "/fvusers/" + this.loggedUser.id + "/img";
		this.userFactory.getImage(this.loggedUser.id).then(response => {

		}, error => {
			this.loggedUser.imgUrl = "assets/imgs/logo-mini.png";
		});
		console.log(this.loggedUser);

		this.missionProvider
			.GetMissions()
			.subscribe(
				res => {
					this.missions = res["rows"]/*.filter(item => (item.FvfavorstateId == 1 && item.FvuserId != this.loggedUser.id))*/;
					console.log("this.missions: ", this.missions);
				},
				err => { console.log('error: ', err); }
			);

		this.missionProvider
			.GetMissionsPossibleTaker()
			.subscribe(
				res => {
					this.salvadiaMissions = res["rows"].filter(item =>
						(item.FvuserId == this.loggedUser.id
							&& item.FvposibletakerstateId == 2));

					console.log("this.salvadiaMissions: ", this.salvadiaMissions);
				},
				err => { console.log('error: ', err); }
			);

		this.userFactory
			.GetUsers()
			.subscribe(
				res => { this.users = res["rows"]; /*console.log(this.users);*/ },
				err => { console.log('error: ', err); }
			);

		this.creditCardProvider
			.GetCreditCards()
			.subscribe(
				resp => {
					this.CreditCards = resp["rows"];
					console.log("this.CreditCards: ", this.CreditCards);
				},
				err => { console.log('error: ', err); }
			);

		this.transactionStateProvider
			.GetTransactions()
			.subscribe(
				resp => {
					this.transactionStates = resp["rows"];
					console.log("this.transactionStates: ", this.transactionStates);
				},
				err => { console.log('error: ', err); }
			);

		this.transactionProvider
			.GetTransactionsByUserId(this.loggedUser.id)
			.subscribe(
				res => {
					this.salvadiaTransactions = res;
					/*console.log("this.salvadiaTransactions: ", this.salvadiaTransactions);

					for (var i = 0; i < this.salvadiaTransactions.length; i++) {

						var keepTransaction = false;

						for (var j = 0; j < this.salvadiaMissions.length; ++j) {
							if (this.salvadiaMissions[j].id = this.salvadiaTransactions[i].FvfavorId) {
								keepTransaction = true;
								break;
							}
						}

						if (!keepTransaction) {
							this.salvadiaTransactions.splice(i, 1);
						}
					}*/
				},
				err => { console.log('error: ', err); }
			);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad SalvadiaTransaccionesPage');
	}
}
