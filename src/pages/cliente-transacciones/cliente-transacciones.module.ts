import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClienteTransaccionesPage } from './cliente-transacciones';

@NgModule({
  declarations: [
    ClienteTransaccionesPage,
  ],
  imports: [
    IonicPageModule.forChild(ClienteTransaccionesPage),
  ],
})
export class ClienteTransaccionesPageModule {}
