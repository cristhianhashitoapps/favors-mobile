import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TransactionProvider } from '../../providers/transaction/transaction';
import { MissionProvider } from '../../providers/mission/mission';
import { CreditCardProvider } from '../../providers/CreditCardProvider';
import { TransactionStateProvider } from '../../providers/transaction-state/transaction-state';

import * as rootScope from '../../app/rootScope';
const $rootScope = rootScope.default;
import { Config } from '../../providers/config';
import { UserFactory } from '../../providers/UserFactory';

@IonicPage()
@Component({
	selector: 'page-cliente-transacciones',
	templateUrl: 'cliente-transacciones.html',
})
export class ClienteTransaccionesPage {

	urlBase: string = Config.ApiUrl;

	private loggedUser;
	private missions;
	private clienteMissions;
	private clienteTransactions;
	private CreditCards;
	private transactionStates;

	constructor(public navCtrl: NavController, public navParams: NavParams,
		public missionProvider: MissionProvider, public creditCardProvider: CreditCardProvider,
		public transactionStateProvider: TransactionStateProvider, public transactionProvider: TransactionProvider,
		private userFactory: UserFactory) {
		this.loggedUser = $rootScope.user;
		this.loggedUser.imgUrl = this.urlBase + "/fvusers/" + this.loggedUser.id + "/img";
		this.userFactory.getImage(this.loggedUser.id).then(response => {

		}, error => {
			this.loggedUser.imgUrl = "assets/imgs/logo-mini.png";
		});
		console.log(this.loggedUser);

		this.creditCardProvider
			.GetCreditCards()
			.subscribe(
				resp => {
					this.CreditCards = resp["rows"];
					console.log("this.CreditCards: ", this.CreditCards);
				},
				err => { console.log('error: ', err); }
			);

		this.transactionStateProvider
			.GetTransactions()
			.subscribe(
				resp => {
					this.transactionStates = resp["rows"];
					console.log("this.transactionStates: ", this.transactionStates);
				},
				err => { console.log('error: ', err); }
			);

		this.missionProvider
			//.GetMissionsPossibleTaker()
			.GetMissions()
			.subscribe(
				res => {
					this.clienteMissions = res["rows"]
					.filter(item => (item.FvuserId == this.loggedUser.id/*&& item.FvposibletakerstateId == 2*/));

					console.log("this.clienteMissions: ", this.clienteMissions);
				},
				err => { console.log('error: ', err); }
			);
/*
		this.missionProvider
			.GetMissions()
			.subscribe(
				res => {
					this.missions = res["rows"].filter(item => (item.FvuserId == this.loggedUser.id) );
					//.filter(item => (item.FvfavorstateId == 1 && item.FvuserId != this.loggedUser.id));
					console.log("this.missions: ", this.missions);
				},
				err => { console.log('error: ', err); }
			);
*/
		this.transactionProvider
			.GetTransactionsByUserId(this.loggedUser.id)
			.subscribe(
				res => {
					this.clienteTransactions = res;
					/*console.log("this.clienteTransactions: ", this.clienteTransactions);

					for (var i = 0; i < this.clienteTransactions.length; i++) {

						var keepTransaction = false;

						for (var j = 0; j < this.clienteMissions.length; ++j) {
							if (this.clienteMissions[j].id = this.clienteTransactions[i].FvfavorId) {
								keepTransaction = true;
								break;
							}
						}

						if (!keepTransaction) {
							this.clienteTransactions.splice(i, 1);
						}
					}*/
				},
				err => { console.log('error: ', err); }
			);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ClienteTransaccionesPage');
	}

}
