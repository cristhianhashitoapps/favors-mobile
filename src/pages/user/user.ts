import { IonicPage, NavController, NavParams, MenuController, AlertController } from 'ionic-angular';
import { Component, Injector } from '@angular/core';
import { ViewController, Events, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UserFactory } from '../../providers/UserFactory';
import * as rootScope from '../../app/rootScope';
import { AuthService } from 'ng2-ui-auth';
import { HomePage } from '../home/home';
import { SalvadiaHomePage } from '../salvadia-home/salvadia-home';
import { SignInPage } from '../sign-in/sign-in-page';
import { ToastController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
const $rootScope = rootScope.default;
import { File, FileEntry } from "@ionic-native/file";
import { Config } from '../../providers/config';
import { DomSanitizer } from '@angular/platform-browser';
import { FavorTypeDto } from '../../dto/FavorTypeDto';
import { FavorTypeService } from '../../providers/FavorTypeService';

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  public urlBase: string = Config.ApiUrl;
  public urlImg: string = Config.ApiImg;


  public image: any;
  public imageFileName: any;
  public user;
  private loader: any;
  private loadingCtrl: LoadingController;
  rootPage: any;
  private typeofuser: boolean;
  public pathImage:any;
  public favorTypes:FavorTypeDto[];
  public favorTypeSelected:any;


  constructor(private events: Events, private viewCtrl: ViewController,
    injector: Injector, public userFactory: UserFactory,
    public auth: AuthService, public navCtrl: NavController,
    public navParams: NavParams, private transfer: FileTransfer,
    private camera: Camera, public toastCtrl: ToastController,
    private file: File, private alertController: AlertController,  
    private sanitizer:DomSanitizer  ,
    private favorTypeService:FavorTypeService
    ) {

    this.loadingCtrl = injector.get(LoadingController);
    this.user = $rootScope.user;
    this.user.password = "";
    
    if ($rootScope.user.typeofuser === 0) {
      this.typeofuser = false;
    } else {
      this.typeofuser = true;
    }

   

    console.log(this.pathImage)

    this.favorTypeService.list().then(res => {      
      this.favorTypes = <FavorTypeDto[]>res["rows"]
      this.favorTypeSelected = this.user.FvfavortypeId;
    });

    this.userFactory.GetUser($rootScope.user.id)
      .subscribe(res=>{
        this.user = res;
        this.user.password = "";
        this.pathImage = this.sanitizer.bypassSecurityTrustUrl([this.urlImg,this.user.img].join('/'));
      }
      ,error=>console.log(error))

  }
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  getImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      this.image = imageData;
      //update image at server
      this.user.img = new Date().getTime() + ".png";
      
      ////////update User with new image name
      this.userFactory
          .UpdateUser(this.user.id,this.user)
          .subscribe(result => {
              ///////upload image
              let options: FileUploadOptions = {
                fileKey: 'userPhoto',
                fileName: this.user.img,
                chunkedMode: false,
                mimeType: "multipart/form-data",
                headers: {},
                params: {}
              }

              const fileTransfer: FileTransferObject = this.transfer.create();

              fileTransfer.upload(this.image, this.urlBase + '/fvusers', options)
              .then((data) => {
                console.log(data + " Uploaded Successfully");
                this.pathImage = this.sanitizer.bypassSecurityTrustUrl([this.urlImg,this.user.img].join('/'));
                $rootScope.user.imgUrl=this.pathImage;
                $rootScope.user.img = this.pathImage;
                console.log("------------------root en user")
                console.log($rootScope.user)
                console.log("------------------root en user")
              }, (err) => {
                console.log(err);
                this.loader.dismiss();
                this.presentToast("Ha ocurrido un error:"+err.error.errors[0].message);
              });

          },error => {
            console.log(error)
          }
        )
      //update image at server
    }, (err) => {
      console.log(err);
      this.presentToast(err);
    });
  }

  enableMenuSwipe() {
    return false;
  }

  ionViewDidLoad() {
  }

  onCancel() {
    this.viewCtrl.dismiss();
  }

  SignUp() {
    const loadingText: String = "Espera un momento";
    this.loader = this.loadingCtrl.create({
      content: `<p class="item">${loadingText}</p>`,
    });
    this.loader.present();

    this.user.FvfavortypeId = this.favorTypeSelected;

    if (this.user.password !== undefined && this.user.password === this.user.passwordConfirm) {



      this.userFactory.UpdateUser(this.user.id, this.user).subscribe(response => {
        let modal = this.alertController.create({
          subTitle: 'Actualización exitosa',
          buttons: ['Terminar'],
          cssClass: "salvadiaConfirm"
        });
        $rootScope.user = response;
        $rootScope.user.imgUrl = this.urlBase + "/fvusers/" + $rootScope.user.id + "/img";
        this.userFactory.getImage($rootScope.user.id).then(response => {

        }, error => {
          $rootScope.user.imgUrl = "assets/imgs/logo-mini.png";
        });
        this.loader.dismiss();
        modal.present();
      });

    } else {
      this.loader.dismiss();
      this.loader = this.loadingCtrl.create({
        content: `<p class="item">Las contraseñas son diferentes</p>`,
      });
      this.loader.present();
      setTimeout(() => {
        this.loader.dismiss();
      }, 2000);
    }


  }

  

}
