import { Component, Output, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UserFactory } from '../../providers/UserFactory';
import { UserDto } from '../../dto/UserDto';
import { PosibleTakerProvider } from '../../providers/PosibleTakerProvider';
import { FavorService as FavorProvider } from '../../providers/FavorService';
import { Config } from '../../providers/config';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  urlBase: string = Config.ApiUrl;

  @Output() viewPostulantsEvent: EventEmitter<number> = new EventEmitter();
  @Output() viewListEvent: EventEmitter<any> = new EventEmitter();
  favorId: number;
  postulantId: number;

  tab = "who";

  user;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private userFactory: UserFactory, private posibleTakerProvider: PosibleTakerProvider,
    private favorProvider: FavorProvider, private alertController: AlertController,
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  changeViewPostulants(favorId: number) {
    this.viewPostulantsEvent.emit(favorId);
  }

  acceptPostulant() {
    console.log("acceptPostulant postulantId " + this.postulantId);
    this.posibleTakerProvider.confirmPostulant(this.postulantId).then((response) => {
      console.log("Confirm ok");
      this.viewListEvent.emit();
      let alert = this.alertController.create({
        title: 'Operación exitosa',
        subTitle: 'El postulante ha sido aceptado',
        buttons: ['Aceptar']
      });
      alert.present();
    });
  }

  viewProfile(postulant: any) {
    this.favorId = postulant.FvfavorId.toString();
    this.postulantId = postulant.id.toString();
    console.log('ProfilePage viewProfile: ' + postulant.FvfavorId.toString());
    this.userFactory.GetUser(postulant.Fvuser.id).subscribe(res => {
      console.log(res);
      this.user = res;
      this.posibleTakerProvider.getStatistics(postulant.Fvuser.id).then((response) => {
        this.user.statistics = response;
      });
      this.posibleTakerProvider.getOpinions(postulant.Fvuser.id).then((response) => {
        this.user.opinions = response;
      });

    }, error => {
      console.log("Error al consultar perfil de usuario id: " + postulant.Fvuser.id);

    });
  }

}
