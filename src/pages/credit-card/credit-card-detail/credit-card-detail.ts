import { Component, EventEmitter, Output } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CreditCardDto } from '../../../dto/CreditCardDto';
import { BankDto } from '../../../dto/BankDto';
import { NavController, NavParams } from 'ionic-angular';
import { BankProvider } from '../../../providers/BankProvider';
import { CreditCardProvider } from '../../../providers/CreditCardProvider';
import { AlertController } from 'ionic-angular';
import { AuthService } from 'ng2-ui-auth';
import * as rootScope from '../../../app/rootScope';

const $rootScope = rootScope.default;

@Component({
  selector: 'credit-card-detail',
  templateUrl: 'credit-card-detail.html'
})
export class CreditCardDetailPage {

  @Output() backToList: EventEmitter<any> = new EventEmitter();
  mask: Array<string | RegExp> = [/\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/];
  cardGroup: FormGroup;
  minYear: string;
  lstBanks: any = { 'rows': null };

  constructor(private formBuilder: FormBuilder, private creditCardProvider: CreditCardProvider,
    private bankProvider: BankProvider, private alertCtrl: AlertController, public authService: AuthService) {
    this.bankProvider = bankProvider;
    this.creditCardProvider = creditCardProvider;
    this.minYear = new Date().toISOString();
    this.cardGroup = this.formBuilder.group({
      franchise: ['', Validators.required],
      number: ['', Validators.compose([Validators.required, Validators.maxLength(19)])],
      expirationMonth: [(new Date('2018-01-01')).toISOString(), Validators.required],
      expirationYear: ['', Validators.required],
      securityCode: ['', Validators.compose([Validators.required, Validators.maxLength(4)])],
      name: ['', Validators.required]
    });
    this.bankProvider.list().then(res => {
      this.lstBanks = res;
    });
    console.log('CreditCardDetailPage loading finished');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreditCardPage');
  }

  public guardar(form): void {
    console.log('Saving credit card ' + $rootScope.user);
    var finaldate = this.cardGroup.value.expirationYear + "/" + this.cardGroup.value.expirationMonth.slice(5, 7);
    var selectedBank = this.lstBanks.rows.find(x => x.id == this.cardGroup.value.franchise);
    var creditCard: CreditCardDto = {
      id: null,
      number: this.cardGroup.value.number,
      securitycode: this.cardGroup.value.securityCode,
      active: "1",
      finaldate: finaldate,
      FvbankId: +this.cardGroup.value.franchise,
      FvbankName: selectedBank.name,
      FvuserId: $rootScope.user.id,
      FvuserName: $rootScope.user.firstname + '-' + $rootScope.user.lastname,
      token: "0",

    };

    this.authService.getToken();
    this.creditCardProvider.create(creditCard).subscribe(
      res => {
        console.log("Operación de tarjeta terminada " + res);
        let alert = this.alertCtrl.create({
          title: 'Operación exitosa',
          subTitle: 'Su tarjeta de crédito ha sido creada exitosamente',
          buttons: ['Aceptar']
        });
        alert.present();
        this.backToList.emit();
      },
      error => {
        console.log("Ha ocurrido un error al registrar la tarjeta");
        let alert = this.alertCtrl.create({
          title: 'Operación erronea',
          subTitle: 'Su tarjeta de crédito no ha pasado el proceso de validación, intente nuevamente',
          buttons: ['Aceptar']
        });
        alert.present();
      }
    );
  }

}
