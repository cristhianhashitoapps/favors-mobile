import { Component } from '@angular/core';
import { AlertController, NavController } from 'ionic-angular';
import { CreditCardPage } from '../../credit-card/credit-card';
import { CreditCardDto } from '../../../dto/CreditCardDto';
import { CreditCardProvider } from '../../../providers/CreditCardProvider';
import { BankProvider } from '../../../providers/BankProvider';
import * as rootScope from '../../../app/rootScope';

const $rootScope = rootScope.default;

@Component({
  selector: 'credit-card-list',
  templateUrl: 'credit-card-list.html'
})
export class CreditCardListPage {

  lstCreditCard: any = {
    'rows': [
      {
        'Fvbank':
        {
          'name': null
        }
      }
    ]
  };

  constructor(private creditCardProvider: CreditCardProvider, private bankProvider: BankProvider,
    public alertCtrl: AlertController, public nav: NavController) {
    console.log('Hello CreditCardListComponent Component');
    this.creditCardProvider = creditCardProvider;
    this.list();
  }

  public list(): void {
    this.creditCardProvider.findByUserId($rootScope.user.id).then(res => {
      this.lstCreditCard = res;

      console.log(this.lstCreditCard.rows);

      for (let creditCard of this.lstCreditCard.rows) {
        creditCard.number = creditCard.number.slice(-4);
      }
    });
  }

  public DeleteCC(cc, ccNumber) {
    let confirm = this.alertCtrl.create({
      title: 'Eliminar tarjeta',
      message: 'Desea eliminar la tarjeta número ****' + ccNumber + '?',
      buttons: [
        { text: 'Cerrar', handler: () => { } },
        {
          text: 'Eliminar',
          handler: () => {

            this.creditCardProvider.delete(cc)
              .subscribe(
                res => {
                  console.log("CreditCardList - DeleteCC");
                  console.log(res);


                  let alert = this.alertCtrl.create({
                    title: 'Operación exitosa',
                    subTitle: 'Su tarjeta de crédito ha sido borrada exitosamente',
                    buttons: ['Aceptar']
                  });
                  alert.present();

                  this.nav.setRoot(CreditCardPage);
                },
                err => {
                  console.log('CreditCardList - DeleteCC - Error: ');
                  console.log(err);
                }
              );
          }
        }]
    });
    confirm.present();
  }

}
