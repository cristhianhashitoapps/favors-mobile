import { Component, ViewChild  } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CreditCardDto } from '../../dto/CreditCardDto';
import { BankDto } from '../../dto/BankDto';
import { NavController, NavParams } from 'ionic-angular';
import { BankProvider } from '../../providers/BankProvider';
import { CreditCardProvider } from '../../providers/CreditCardProvider';

import * as rootScope from '../../app/rootScope';
const $rootScope = rootScope.default;

@Component({
  selector: 'page-credit-card',
  templateUrl: 'credit-card.html',
})
export class CreditCardPage {

  @ViewChild('ccl') ccl;
  @ViewChild('ccd') ccd;

  loggedUser: any = { 'firstname': '', 'lastname': '' };
  detailActive: boolean = false;
  listActive: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.listActive = true;
    console.log('loading CreditCardPage');

    this.loggedUser = $rootScope.user;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreditCardPage');
  }

  public activeDetails(){
    this.detailActive = true;
    this.listActive = false;
  };

  public volver(){
    console.log('CreditCardPage volver');
    this.detailActive = false;
    this.listActive = true;
  };

}
