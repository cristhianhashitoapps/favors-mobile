import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavorListModalPage } from './favor-list-modal';

@NgModule({
  declarations: [
    FavorListModalPage,
  ],
  imports: [
    IonicPageModule.forChild(FavorListModalPage),
  ],
})
export class FavorListModalPageModule {}
