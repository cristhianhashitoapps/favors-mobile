import { IonicPage, NavController, NavParams, MenuController, AlertController } from 'ionic-angular';
import { Component, Injector, OnInit } from '@angular/core';
import { ViewController, Events, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UserFactory } from '../../providers/UserFactory';
import * as rootScope from '../../app/rootScope';
import { AuthService } from 'ng2-ui-auth';
import { HomePage } from '../home/home';
import { SalvadiaHomePage } from '../salvadia-home/salvadia-home';
import { SignInPage } from '../sign-in/sign-in-page';
import { ToastController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
const $rootScope = rootScope.default;
import { File, FileEntry } from "@ionic-native/file";
import { Config } from '../../providers/config';
import { FavorTypeDto } from '../../dto/FavorTypeDto';
import { FavorTypeService } from '../../providers/FavorTypeService';

@IonicPage()
@Component({
  selector: 'page-sign-up-page',
  templateUrl: 'sign-up-page.html'
})
export class SignUpPage{

  public urlBase: string = Config.ApiUrl;

  image: any;
  imageFileName: any;
  public user;
  private loader: any;
  private loadingCtrl: LoadingController;
  rootPage: any;
  private typeofuser: boolean;
  public favorTypes:FavorTypeDto[]
  public favorTypeSelected:any = 1


  constructor(
    private events: Events,
    private viewCtrl: ViewController,
    injector: Injector,
    public userFactory: UserFactory,
    public auth: AuthService,
    public navCtrl: NavController,
    public navParams: NavParams,
    private transfer: FileTransfer,
    private camera: Camera,
    public toastCtrl: ToastController,
    private file: File,
    private alertController: AlertController,
    private favorTypeService: FavorTypeService
  ) {
    this.loadingCtrl = injector.get(LoadingController);
    this.user = {};
    this.typeofuser = this.navParams.data.salvadia;

    this.favorTypeService.list().then(res => {      
      this.favorTypes = <FavorTypeDto[]>res["rows"]
      console.log(this.favorTypes)
    });

  }
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  getImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      this.image = imageData;
    }, (err) => {
      console.log(err);
      this.presentToast(err);
    });
  }

  enableMenuSwipe() {
    return false;
  }

  ionViewDidLoad() {
  }

  onCancel() {
    this.viewCtrl.dismiss();
  }

  SignUp() {

    console.log(this.favorTypeSelected)
    const loadingText: String = "Espera un momento";
    this.loader = this.loadingCtrl.create({
      content: `<p class="item">${loadingText}</p>`,
    });
    this.loader.present();

    if (this.user.password !== undefined && this.user.password === this.user.passwordConfirm) {
      this.user.typeofuser = this.typeofuser;


      const fileTransfer: FileTransferObject = this.transfer.create();
      this.user.typeofuser = this.typeofuser;
      this.user.img = new Date().getTime() + ".png";
      this.user.active=0;
      this.user.FvfavortypeId=this.favorTypeSelected

      //enviar tokenfcm
      this.user.tokenfcm = $rootScope.tokenfcm;

      if (this.image === undefined) {
        
        
        this.userFactory
          .SignUp(this.user)
          .subscribe(result => {

            this.loader.dismiss();
            if (this.user.typeofuser === false) {
              let modal = this.alertController.create({
                subTitle: 'Registro exitoso',
                buttons: ['Terminar'],
                cssClass: "salvadiaConfirm"
              });
              modal.present();
            } else {
              let modal = this.alertController.create({
                subTitle: 'Gracias por registrarte. Un agente se pondrá en contacto contigo para hacerte una entrevista y terminar el proceso',
                buttons: ['Terminar'],
                cssClass: "salvadiaConfirm"
              });
              modal.present();
            }

            $rootScope.user = this.user;
            this.auth.setToken(result["Token"]);
            $rootScope.user.id = this.auth.getPayload().sub.id;
            console.log("----------USER-CUANDO REGISTRA----RESPUESTA SERVER------------");
            console.log($rootScope.user);
            console.log($rootScope.user.active);
            console.log("----------USER-----------------");


            if ($rootScope.user.typeofuser === false) {
              this.navCtrl.setRoot(HomePage);
              this.events.publish('user:logged', 0);
            } else {
              this.auth.logout();
              this.navCtrl.setRoot(SignInPage);
              this.events.publish('user:logged', 3);
            }
            this.user = {};

          }, error => {
            console.log(error);
            this.loader.dismiss();
            this.presentToast("Ha ocurrido un error: "+error.error.errors[0].message);
          });

      } else {

        this.userFactory
          .SignUp(this.user)
          .subscribe(result => {

            let options: FileUploadOptions = {
              fileKey: 'userPhoto',
              fileName: this.user.img,
              chunkedMode: false,
              mimeType: "multipart/form-data",
              headers: {},
              params: {}
            }

            $rootScope.user = this.user;
            this.auth.setToken(result["Token"]);
            $rootScope.user.id = this.auth.getPayload().sub.id;
            console.log("----------USER-CUANDO REGISTRA----RESPUESTA SERVER------------");
            console.log($rootScope.user);
            console.log("----------USER-----------------");

            fileTransfer.upload(this.image, this.urlBase + '/fvusers', options)
              .then((data) => {
                console.log(data + " Uploaded Successfully");


                this.loader.dismiss();
                if (this.user.typeofuser === false) {
                  let modal = this.alertController.create({
                    subTitle: 'Registro exitoso',
                    buttons: ['Terminar'],
                    cssClass: "salvadiaConfirm"
                  });
                  modal.present();
                } else {
                  let modal = this.alertController.create({
                    subTitle: 'Gracias por registrarte. Un agente se pondrá en contacto contigo para hacerte una entrevista y terminar el proceso',
                    buttons: ['Terminar'],
                    cssClass: "salvadiaConfirm"
                  });
                  modal.present();
                }

                //$rootScope.user = this.user;

                if ($rootScope.user.typeofuser === false) {
                  this.navCtrl.setRoot(HomePage);
                  this.events.publish('user:logged', 0);
                } else {
                  this.auth.logout();
                  this.navCtrl.setRoot(SignInPage);//tiene que ir a loguearse
                  this.events.publish('user:logged', 3);
                }
                this.user = {};


              }, (err) => {
                console.log(err);
                this.loader.dismiss();
                this.presentToast("Ha ocurrido un error:"+err.error.errors[0].message);
              });



          }, error => {
            console.log(error);
            this.loader.dismiss();
            this.presentToast("Ha ocurrido un error: "+error.error.errors[0].message);
          });

      }


    } else {
      this.loader.dismiss();
      this.loader = this.loadingCtrl.create({
        content: `<p class="item">Las contraseñas son diferentes</p>`,
      });
      this.loader.present();
      setTimeout(() => {
        this.loader.dismiss();
      }, 2000);
    }


  }

  GoToSignIn() {
    const loadingText: String = "Espera un momento";
    this.loader = this.loadingCtrl.create({
      content: `<p class="item">${loadingText}</p>`,
    });
    this.navCtrl.setRoot(SignInPage);
    this.loader.present();
    this.loader.dismiss();
  }

  /*ngOnInit(){
    console.log("inicio")
    this.favorTypeService.list().then(res => {      
      this.favorTypes = <FavorTypeDto[]>res["rows"]
      console.log(this.favorTypes)
    });
  }*/

}
