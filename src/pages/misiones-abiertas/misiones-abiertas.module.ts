import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisionesAbiertasPage } from './misiones-abiertas';

@NgModule({
  declarations: [
    MisionesAbiertasPage,
  ],
  imports: [
    IonicPageModule.forChild(MisionesAbiertasPage),
  ],
})
export class MisionesAbiertasPageModule {}
