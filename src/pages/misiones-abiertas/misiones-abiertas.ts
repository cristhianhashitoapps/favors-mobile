import { Component } from '@angular/core';
import { App, AlertController, Events } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Loading, LoadingController } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from 'ng2-ui-auth';

import { MissionProvider } from '../../providers/mission/mission';
import { TypesProvider } from '../../providers/types/types';
import { UserFactory } from '../../providers/UserFactory';
import { CityProvider } from '../../providers/CityProvider';
import { AddressProvider } from '../../providers/AddressProvider';
import { AddressFieldProvider } from '../../providers/AddressFieldProvider';
import { GeolocationProvider } from '../../providers/GeolocationProvider';
import { SignInPage } from '../sign-in/sign-in-page';

import * as rootScope from '../../app/rootScope';
const $rootScope = rootScope.default;
import { Config } from '../../providers/config';

@IonicPage()
@Component({
	selector: 'page-misiones-abiertas',
	templateUrl: 'misiones-abiertas.html',
})
export class MisionesAbiertasPage {

	urlBase: string = Config.ApiUrl;

	private missionsPosibleTakers;
	private missionTypes;
	private missions;
	private types;
	private users;
	private loggedUser;
	private addresses;
	private addressFields;
	private cities;
	private usuarioLogeado;

	constructor(public navCtrl: NavController, public navParams: NavParams,
		public missionProvider: MissionProvider, public userFactory: UserFactory,
		public typesProvider: TypesProvider, public alertCtrl: AlertController,
		private geolocation: Geolocation, private geolocationProvider: GeolocationProvider,
		public addressProvider: AddressProvider, public addressFieldProvider: AddressFieldProvider,
		public cityProvider: CityProvider, public loadingCtrl: LoadingController,
		public auth: AuthService, public events: Events, public appCtrl: App) {

		this.loggedUser = $rootScope.user;
		this.loggedUser.imgUrl = this.urlBase + "/fvusers/" + this.loggedUser.id + "/img";
		this.userFactory.getImage(this.loggedUser.id).then(response => {

		}, error => {
			this.loggedUser.imgUrl = "assets/imgs/logo-mini.png";
		});
		
		//cambio jk
		/*
		console.log("----- misiones-abiertas.ts - this.loggedUser -----");
		console.log(this.loggedUser);
		console.log("-------------------------");
		*/
		this.userFactory
			.GetUsers()
			.subscribe(
				res => { 
					this.users = res["rows"];
					this.usuarioLogeado = res["rows"].filter(item => (item.id == this.loggedUser.id));
					/*
					console.log("----- misiones-abiertas.ts - this.users -----");
					console.log(this.users);
					console.log("-------------------------");
					*/
					if(!this.usuarioLogeado[0].active){
						let modal = this.alertCtrl.create({
							title: 'Usuario no activo',
							subTitle: 'Por favor comunicate con Salvadia Inc.',
						 	buttons: [
						 		{
						 			text: 'Cerrar',
						 			role: 'cerrar',
						 			handler: () => {
						 				console.log('Haciendo logout, redireccionando al HomePage');
						 				this.auth.logout();
						 				this.appCtrl.getRootNav().setRoot(SignInPage);
						 				this.events.publish('user:logged', 3);
						 			}
						 		}
						 	],
						 	enableBackdropDismiss: false
						});

						modal.present();
					}
				},
				err => { console.log('error: ', err); }
			);

		this.missionProvider
			.GetMissionsPossibleTaker()
			.subscribe(
				res => {
					this.missionsPosibleTakers = res["rows"].filter(item => (item.FvuserId == this.loggedUser.id));
					console.log("GetMissionsPossibleTaker", this.missionsPosibleTakers);

				},
				err => { console.log('error: ', err); }
			);

		this.userFactory
			.GetUsers()
			.subscribe(
				res => { this.users = res["rows"]; /*console.log(this.users);*/ },
				err => { console.log('error: ', err); }
			);

		this.addressProvider
			.GetAddresses()
			.subscribe(
				res => { this.addresses = res["rows"]; console.log("addressProvider-GetAddresses", this.addresses); },
				err => { console.log('error: ', err); }
			);

		this.addressFieldProvider
			.list()
			.then(
				res => {
					this.addressFields = res.rows;
					console.log("addressFieldProvider-list", this.addressFields);
				}
			);

		this.cityProvider
			.list()
			.then(
				res => {
					this.cities = res.rows;
					console.log("cityProvider-list", this.cities);
				},
				err => { console.log('error: ', err); }
			);



		this.missionProvider
			.GetMissions()
			.subscribe(
				res => {
					this.missions = res["rows"].filter(item => (item.FvfavorstateId == 1 && item.FvuserId != this.loggedUser.id));
					console.log("misiones---------------------------------------------")
					console.log(this.missions);
					console.log("misiones---------------------------------------------")

					Object.keys(this.missionsPosibleTakers).forEach(keyMPT => {

						Object.keys(this.missions).forEach(keyM => {

							if (this.missions[keyM] !== undefined && this.missionsPosibleTakers[keyMPT].FvfavorId == this.missions[keyM].id) {
								this.missions.splice(keyM, 1);
							}
						});

					});
				},
				err => { console.log('error: ', err); }
			);


		this.typesProvider
			.GetTypes()
			.subscribe(
				res => { this.types = res["rows"]; console.log("typesProvider-GetTypes", this.types); },
				err => { console.log('error: ', err); }
			);

		this.typesProvider
			.GetMissionTypes()
			.subscribe(
				res => { this.missionTypes = res["rows"]; console.log("typesProvider-GetMissionTypes", this.missionTypes); },
				err => { console.log('error: ', err); }
			);
		this.updateClosestMissions();
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad MisionesAbiertasPage');
	}

	confirmMission(task, favorId, salvadiaId) {
		let confirm = this.alertCtrl.create({
			title: 'Desea postularse a la siguiente misión?',
			message: task,
			buttons: [
				{ text: 'Cerrar', handler: () => { } },
				{
					text: 'Postularse',
					handler: () => {

						var data = {
							"efective": false,
							"createdAt": new Date().toISOString(),
							"updatedAt": new Date().toISOString(),
							"FvfavorstateId": 1,
							"FvfavorId": favorId,
							"FvuserId": salvadiaId
						}

						this.missionProvider
							.CreateNomination(data)
							.subscribe((result: any) => {
								//console.log("Resultado: ",result);
								this.navCtrl.parent.select(1);
							})
						err => {
							console.log('error: ', err);
						}
					}
				}]
		});
		confirm.present();
	}

	haversine(lat1, long1, lat2, long2) {
		/*
		a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
	c = 2 ⋅ atan2( √a, √(1−a) )
	d = R ⋅ c
	*/

		var R = 6371e3; // metres
		var φ1 = lat1 * (Math.PI / 180);
		var φ2 = lat2 * (Math.PI / 180);
		var Δφ = (lat2 - lat1) * (Math.PI / 180);
		var Δλ = (long2 - long1) * (Math.PI / 180);

		var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
			Math.cos(φ1) * Math.cos(φ2) *
			Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		var d = R * c;

		return d;
	}

	updateClosestMissions() {
		/*
				this.loadingCtrl.create({
					spinner: 'hide',
					content: "Cargando misiones en su sector...",
					duration: 7500
				}).present();
		
				this.geolocation.getCurrentPosition().then((resp) => {
					Object.keys(this.missions).forEach(key=> {
						var address = this.addresses.filter(item => item.id == this.missions[key].FvaddressId);
						var distance = this.haversine(resp.coords.latitude, resp.coords.longitude, address[0].lat, address[0].lang);
		
						//console.log(resp.coords.latitude+","+resp.coords.longitude+" : "+address[0].lat+","+address[0].lang + " = " + distance);
		
						if(distance>1500){
						//if(distance>3){
							this.missions.splice(key,1);
						}
					});
				})
				.catch((error) => {
					console.log('Error getting location', error);
				});
		*/
	}
}
